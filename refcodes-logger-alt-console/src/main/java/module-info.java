module org.refcodes.logger.alt.console {
	requires org.refcodes.data;
	requires org.refcodes.runtime;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.logger;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.tabular;
	requires transitive org.refcodes.textual;

	exports org.refcodes.logger.alt.console;
}
