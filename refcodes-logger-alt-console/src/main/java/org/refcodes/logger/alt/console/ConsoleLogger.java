// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.logger.alt.console;

import java.io.PrintStream;

import org.refcodes.data.ConsoleDimension;
import org.refcodes.data.Field;
import org.refcodes.logger.ColumnLayout;
import org.refcodes.logger.ColumnLayoutAccessor.ColumnLayoutBuilder;
import org.refcodes.logger.ColumnLayoutAccessor.ColumnLayoutProperty;
import org.refcodes.logger.IllegalRecordRuntimeException;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.LoggerField;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.logger.RuntimeLoggerSingleton;
import org.refcodes.runtime.EnvironmentVariable;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.runtime.Terminal;
import org.refcodes.tabular.ColumnMismatchException;
import org.refcodes.tabular.FormattedColumn;
import org.refcodes.tabular.FormattedHeader;
import org.refcodes.tabular.Record;
import org.refcodes.textual.MoreTextMode;
import org.refcodes.textual.TableBuilder;
import org.refcodes.textual.TableStyle;

/**
 * Specialized subclass of the {@link AbstractConsoleLogger} for handling
 * {@link RuntimeLogger}'s logs with ANSI Escape-Codes.
 */
public class ConsoleLogger extends AbstractConsoleLogger<Object> implements ColumnLayoutProperty, ColumnLayoutBuilder<ConsoleLogger> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	static {
		RuntimeLoggerFactorySingleton.bindJavaUtilLogging();
	}

	// /////////////////////////////////////////////////////////////////////////
	// ENUM:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Enum TableLayout.
	 */
	private enum TableLayout {
		NONE, MESSAGE, EXCEPTION
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_STACKTRACE_INSIDE_TABLE = true;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LogPriority _lastPriority;
	private int _exceptionIndex;
	private FormattedColumn<Throwable> _errorColumn;
	protected TableBuilder _errorBuilder;
	protected TableBuilder _standardBuilder;
	protected FormattedHeader<Object> _standardHeader;
	private TableLayout _lastLayout = TableLayout.NONE;
	private ColumnLayout _columnLayout = ColumnLayout.NONE;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new console logger impl.
	 */
	public ConsoleLogger() {
		this( toLoggerLayout( null ) );
	}

	/**
	 * Instantiates a new console logger impl.
	 *
	 * @param aLoggerLayout the logger layout
	 */
	public ConsoleLogger( ColumnLayout aLoggerLayout ) {
		super( new ConsoleLoggerHeader( toLoggerLayout( aLoggerLayout ) ) );
		init( toLoggerLayout( aLoggerLayout ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnLayout getColumnLayout() {
		return _columnLayout;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnLayout( ColumnLayout aColumnLayout ) {
		if ( toLoggerLayout() == null ) {
			final ColumnLayout theColumnLayout = toLoggerLayout( aColumnLayout );
			if ( theColumnLayout != _columnLayout ) {
				_columnLayout = theColumnLayout;
				_errorColumn = null;
				_header = new ConsoleLoggerHeader( _columnLayout );
				init();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void log( Record<? extends Object> aRecord ) {
		try {
			final LogPriority thePriority = (LogPriority) LoggerField.LOG_PRIORITY.getColumn().get( aRecord );

			// Do we have an exception? |-->
			Throwable theException = null;
			if ( aRecord.size() > _exceptionIndex ) {
				theException = (Throwable) aRecord.get( Field.LOG_EXCEPTION.getName() );
			}
			// <--| Do we have an exception?

			// Doing logger's state brainf*ck |-->
			if ( thePriority == null || thePriority.getPriority() > LogPriority.DISCARD.getPriority() ) {
				if ( _lastPriority != thePriority || _columnLayout == ColumnLayout.ANALYST || thePriority.getPriority() >= LogPriority.WARN.getPriority() || _isPrintSeparator ) {
					_isPrintSeparator = false;
					if ( _lastLayout == TableLayout.EXCEPTION ) {
						if ( theException != null ) {
							if ( IS_STACKTRACE_INSIDE_TABLE ) {
								_standardBuilder.printRowEnd( _errorBuilder );
							}
							else {
								if ( !_isPrintHead ) {
									_standardBuilder.printRowEnd( _errorBuilder );
								}

							}
						}
						else {
							if ( IS_STACKTRACE_INSIDE_TABLE ) {
								if ( thePriority.getPriority() >= LogPriority.WARN.getPriority() ) {
									_standardBuilder.printRowEnd( _errorBuilder );
								}
								else {
									_tableBuilder.printRowEnd( _errorBuilder );
								}
							}
						}
					}
					else if ( _lastLayout != TableLayout.NONE ) {
						_tableBuilder.printRowBegin();
					}
				}
				if ( thePriority.getPriority() >= LogPriority.WARN.getPriority() ) {
					if ( _lastLayout == TableLayout.NONE && _hasLogLines ) {
						_standardBuilder.printRowBegin();
					}
					log( aRecord, _standardHeader, _standardBuilder );
				}
				else {
					super.log( aRecord );
				}

				if ( theException != null ) {
					printException( theException );
					_lastLayout = TableLayout.EXCEPTION;
				}
				else {
					_lastLayout = TableLayout.MESSAGE;
				}
				// <--| Doing logger's state brainf*ck
			}

			// Log exceptions even if logs are disabled |-->
			else {
				if ( theException != null ) {
					System.err.println( theException.getMessage() );
					theException.printStackTrace();
				}
			}
			// <--| Log exceptions even if logs are disabled
			_lastPriority = thePriority;
		}
		catch ( ColumnMismatchException | ClassCastException e ) {
			throw new IllegalRecordRuntimeException( aRecord, e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printTail() {
		if ( _hasLogLines ) {
			if ( IS_STACKTRACE_INSIDE_TABLE ) {
				if ( _lastLayout == TableLayout.EXCEPTION ) {
					_errorBuilder.printTail();
					_errStream.flush();
				}
				else {
					super.printTail();
				}
			}
			else {
				super.printTail();
			}
			_lastLayout = TableLayout.NONE;
			_hasLogLines = false;
			// Uncomment to do an "auto-print-head" upon a printTail() call |-->
			_isPrintHead = true;
			// Uncomment to do an "auto-print-head" upon a printTail() call <--|
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLogger withColumnLayout( ColumnLayout aColumnLayout ) {
		setColumnLayout( aColumnLayout );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLogger withTableStyle( TableStyle aTableStyle ) {
		setTableStyle( aTableStyle );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLogger withRowWidth( int aRowWidth ) {
		setRowWidth( aRowWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLogger withEscapeCodes( boolean isEscCodesEnabled ) {
		setEscapeCodes( isEscCodesEnabled );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLogger withLeftBorder( boolean hasLeftBorder ) {
		setLeftBorder( hasLeftBorder );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLogger withRightBorder( boolean hasRightBorder ) {
		setRightBorder( hasRightBorder );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLogger withErrorPrintStream( PrintStream aErrorPrintStream ) {
		setErrorPrintStream( aErrorPrintStream );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLogger withStandardPrintStream( PrintStream aStandardPrintStream ) {
		setStandardPrintStream( aStandardPrintStream );
		return this;
	}

	/**
	 * As the underlying configuration framework used by the
	 * {@link RuntimeLoggerSingleton} cannot convert a String to an enum, we got
	 * to provide such a method ourselves. When configuring programmatically use
	 * {@link #setColumnLayout(ColumnLayout)}, when using the
	 * "<code>runtimelogger-config.xml</code>" use the property "loggerLayout"
	 * for this method to be invoked. Valid arguments are as of the
	 * {@link ColumnLayout} enumeration:
	 * <ul>
	 * <li>GRANDPA</li>
	 * <li>SUPERUSER</li>
	 * <li>FALLBACK</li>
	 * <li>DEVELOPER</li>
	 * <li>DEVOPS</li>
	 * <li>ENDUSER</li>
	 * <li>ANALYST</li>
	 * </ul>
	 *
	 * @param aLoggerLayout the new logger layout
	 */
	public void setLoggerLayout( String aLoggerLayout ) {
		setColumnLayout( ColumnLayout.toColumnLayout( aLoggerLayout ) );
	}

	/**
	 * Convenience method for {@link #setLoggerLayout(String)}:
	 * 
	 * As the underlying configuration framework used by the
	 * {@link RuntimeLoggerSingleton} cannot convert a String to an enum, we got
	 * to provide such a method ourselves. When configuring programmatically use
	 * {@link #setColumnLayout(ColumnLayout)}, when using the
	 * "<code>runtimelogger-config.xml</code>" use the property "loggerLayout"
	 * for this method to be invoked. Valid arguments are as of the
	 * {@link ColumnLayout} enumeration:
	 * <ul>
	 * <li>GRANDPA</li>
	 * <li>SUPERUSER</li>
	 * <li>FALLBACK</li>
	 * <li>DEVELOPER</li>
	 * <li>DEVOPS</li>
	 * <li>ENDUSER</li>
	 * <li>ANALYST</li>
	 * </ul>
	 *
	 * @param aLoggerLayout the new logger layout
	 */
	public void setLayout( String aLoggerLayout ) {
		setLoggerLayout( aLoggerLayout );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines the {@link ColumnLayout} by evaluating the
	 * {@link SystemProperty#LOGGER_LAYOUT} and the
	 * {@link EnvironmentVariable#LOGGER_LAYOUT} (in this order). If none
	 * (valid) layout was determinable, then the {@link ColumnLayout#SUPERUSER}
	 * is returned.
	 *
	 * @param aLoggerLayout the logger layout
	 * 
	 * @return The {@link ColumnLayout} to be used.
	 */
	protected static ColumnLayout toLoggerLayout( ColumnLayout aLoggerLayout ) {
		if ( aLoggerLayout != null ) {
			if ( aLoggerLayout != ColumnLayout.SUPERUSER || Terminal.toHeuristicWidth() >= ConsoleDimension.NORM_WIDTH.getValue() ) {
				return aLoggerLayout;
			}
		}
		ColumnLayout theLoggerLayout = toLoggerLayout();
		if ( theLoggerLayout == null ) {
			theLoggerLayout = ColumnLayout.SUPERUSER;
		}
		if ( theLoggerLayout == ColumnLayout.SUPERUSER && Terminal.toHeuristicWidth() < ConsoleDimension.NORM_WIDTH.getValue() ) {
			return ColumnLayout.BASIC;
		}
		return theLoggerLayout;
	}

	/**
	 * To logger layout.
	 *
	 * @return the column layout
	 */
	protected static ColumnLayout toLoggerLayout() {
		ColumnLayout theLoggerLayout = null;
		final String theResult = SystemProperty.toPropertyValue( SystemProperty.LOGGER_LAYOUT, EnvironmentVariable.LOGGER_LAYOUT );
		if ( theResult != null ) {
			try {
				theLoggerLayout = ColumnLayout.toColumnLayout( theResult );
				if ( theLoggerLayout == null ) {
					theLoggerLayout = ColumnLayout.SUPERUSER;
				}
			}
			catch ( IllegalArgumentException | NullPointerException e ) {}
		}
		return theLoggerLayout;
	}

	/**
	 * Determines the {@link TableStyle} by evaluating the
	 * {@link SystemProperty#LOGGER_STYLE} and the
	 * {@link EnvironmentVariable#LOGGER_STYLE} (in this order). If none (valid)
	 * style was determinable, then a proposed style is returned.
	 * 
	 * @return The {@link TableStyle} to be used.
	 */
	protected static TableStyle toShortcutLoggerStyle() {
		final String theResult = SystemProperty.toPropertyValue( SystemProperty.LOGGER_STYLE, EnvironmentVariable.LOGGER_STYLE );
		if ( theResult != null ) {
			try {
				final TableStyle theTableStyle = TableStyle.toTableStyle( theResult );
				return theTableStyle;
			}
			catch ( IllegalArgumentException | NullPointerException e ) {}
		}
		return null;
	}

	/**
	 * Prints the exception.
	 *
	 * @param theException the the exception
	 */
	protected synchronized void printException( Throwable theException ) {
		if ( IS_STACKTRACE_INSIDE_TABLE ) {
			String theText = _errorColumn.toPrintable( theException );
			theText = theText.replaceAll( "\t", "--> " );
			_errorBuilder.printRowEnd( _standardBuilder );
			_errorBuilder.printRowContinue( theText );
			_errorBuilder.withPrintStream( _errStream );
		}
		else {
			printTail();
			theException.printStackTrace( _errStream );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected void init() {
		final TableStyle theTableStyle = toShortcutLoggerStyle();
		if ( theTableStyle != null ) {
			_tableStyle = theTableStyle;
		}
		if ( _errorColumn == null ) {
			_exceptionIndex = _header.indexOf( Field.LOG_EXCEPTION.getName() );
			_errorColumn = (FormattedColumn<Throwable>) _header.delete( Field.LOG_EXCEPTION.getName() );
		}
		_errorBuilder = new TableBuilder( _rowWidth ).addColumn().withColumnFormatMetrics( _errorColumn ).withPrintStream( _errStream ).withTableStyle( _tableStyle ).withLeftBorder( _hasLeftBorder ).withRightBorder( _hasRightBorder );
		_errorBuilder.withBorderEscapeCode( DEFAULT_ANSI_BOX_GRID_COLOR );
		_errorBuilder.withLineBreak( Terminal.toLineBreak( _rowWidth ) );
		if ( _columnLayout != null ) {
			init( _columnLayout );
		}
		super.init();
	}

	/**
	 * Inits the.
	 *
	 * @param aColumnLayout the column layout
	 */
	private void init( ColumnLayout aColumnLayout ) {
		_columnLayout = aColumnLayout;
		_standardHeader = toVerboseHeader();
		_standardBuilder = toPreConfiguredTableBuilder( _standardHeader, OutputPrintStream.ERROR );
	}

	/**
	 * To verbose header.
	 *
	 * @return the formatted header
	 */
	private FormattedHeader<Object> toVerboseHeader() {
		final FormattedHeader<Object> theVerboseHeader = new ConsoleLoggerHeader( _columnLayout );
		final FormattedColumn<?> theMessageColumn = _header.get( Field.LOG_MESSAGE.getName() );
		theMessageColumn.setHeaderMoreTextMode( MoreTextMode.NONE );
		theVerboseHeader.delete( Field.LOG_EXCEPTION.getName() );
		return theVerboseHeader;
	}
}
