// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.console;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.refcodes.component.Destroyable;
import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.data.Text;
import org.refcodes.logger.IllegalRecordRuntimeException;
import org.refcodes.logger.Logger;
import org.refcodes.logger.RuntimeLoggerSingleton;
import org.refcodes.mixin.ErrorPrintStreamAccessor.ErrorPrintStreamBuilder;
import org.refcodes.mixin.ErrorPrintStreamAccessor.ErrorPrintStreamProperty;
import org.refcodes.mixin.RowWidthAccessor.RowWidthBuilder;
import org.refcodes.mixin.RowWidthAccessor.RowWidthProperty;
import org.refcodes.mixin.StandardPrintStreamAccessor.StandardPrintStreamBuilder;
import org.refcodes.mixin.StandardPrintStreamAccessor.StandardPrintStreamProperty;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.runtime.Terminal;
import org.refcodes.tabular.ColumnMismatchException;
import org.refcodes.tabular.FormattedHeader;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.HeaderMismatchException;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.Row;
import org.refcodes.textual.ColumnSetupMetrics;
import org.refcodes.textual.TableBuilder;
import org.refcodes.textual.TableStatus;
import org.refcodes.textual.TableStyle;
import org.refcodes.textual.TableStyleAccessor.TableStyleBuilder;
import org.refcodes.textual.TableStyleAccessor.TableStyleProperty;

/**
 * The {@link AbstractConsoleLogger} implements the {@link Logger} interface for
 * providing logging functionality with extended pimped console output (via
 * {@link System#out} and (via sub-classing also {@link System#err}).
 * <p>
 * The {@link AbstractConsoleLogger} by default uses the most promising width in
 * characters of the system's terminal in use by calling the method
 * <code>SystemUtility.toPreferredTerminalWidth()</code>. In case you pass a
 * "-Dconsole.width=n" JVM argument, then your width is taken, else the actual
 * console's width is being tried to be determined. See
 * {@link SystemProperty#CONSOLE_WIDTH}. You can also use the
 * {@link #setRowWidth(int)} or {@link #withRowWidth(int)} method in order to
 * programmatically set the console's row width.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public abstract class AbstractConsoleLogger<T> implements Destroyable, Logger<T>, RowWidthProperty, RowWidthBuilder<AbstractConsoleLogger<?>>, TableStyleProperty, TableStyleBuilder<AbstractConsoleLogger<?>>, StandardPrintStreamProperty, StandardPrintStreamBuilder<AbstractConsoleLogger<T>>, ErrorPrintStreamProperty, ErrorPrintStreamBuilder<AbstractConsoleLogger<T>> {

	// /////////////////////////////////////////////////////////////////////////
	// ENUM:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Enum OutputPrintStream.
	 */
	public enum OutputPrintStream {
		STANDARD, ERROR
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The default ANSI Escape-Sequence for the logger's box grid to be used
	 * when ANSI Escape-Codes are enabled.
	 */
	protected String DEFAULT_ANSI_BOX_GRID_COLOR = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.DEFAULT_FOREGROUND_COLOR, AnsiEscapeCode.FAINT );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected boolean _isPrintHead = true;
	protected boolean _isPrintSeparator = false;
	protected boolean _hasLogLines = false;
	protected FormattedHeader<T> _header;
	protected TableBuilder _tableBuilder;
	protected PrintStream _stdStream = Execution.toSystemOut();
	protected PrintStream _errStream = Execution.toSystemErr();
	protected int _rowWidth = Terminal.toHeuristicWidth();
	protected TableStyle _tableStyle;
	protected boolean _hasLeftBorder = true;
	protected boolean _hasRightBorder = true;
	private List<String> _columnNames;
	private boolean _isEscCodesEnabled = Terminal.isAnsiTerminalEnabled();

	// /////////////////////////////////////////////////////////////////////////
	// STATE:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Initially enables or disables ANSI escape sequences as of detection of
	 * terminal's ANSI support. You can overrule this setting by calling
	 * {@link #setEscapeCodes(boolean)}. See also
	 * <code>SystemUtility.isAnsiTerminal()</code>.
	 *
	 * @param aHeader the header
	 */
	public AbstractConsoleLogger( FormattedHeader<T> aHeader ) {
		_header = aHeader;
		_tableStyle = TableStyle.toExecutionTableStyle();
		init();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void log( Record<? extends T> aRecord ) {
		log( aRecord, _header, _tableBuilder );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream getStandardPrintStream() {
		return _stdStream;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStandardPrintStream( PrintStream aOutStream ) {
		_stdStream = aOutStream;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream getErrorPrintStream() {
		return _errStream;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setErrorPrintStream( PrintStream aErrStream ) {
		_errStream = aErrStream;
	}

	/**
	 * Convenience method for {@link #setLoggerStyle(String)}:
	 * 
	 * As the underlying configuration framework used by the
	 * {@link RuntimeLoggerSingleton} cannot convert a String to an enum, we got
	 * to provide such a method ourselves. When configuring programmatically use
	 * {@link #setTableStyle(TableStyle)}, when using the
	 * "<code>runtimelogger-config.xml</code>" use the property "loggerStyle"
	 * for this method to be invoked. Valid arguments are as of the
	 * {@link TableStyle} enumeration:
	 * <ul>
	 * <li>SINGLE_HEADER_SINGLE_BODY</li>
	 * <li>DOUBLE_SINGLE_HEADER_SINGLE_BODY</li>
	 * <li>DOUBLE_HEADER_DOUBLE_BODY</li>
	 * <li>DOUBLE_SINGLE_HEADER_DOUBLE_SINGLE_BODY</li>
	 * <li>DOUBLE_HEADER_SINGLE_BODY</li>
	 * <li>DOUBLE_HEADER_DOUBLE_SINGLE_BODY</li>
	 * <li>DOUBLE_SINGLE_HEADER_SINGLE_DASHED_BODY</li>
	 * <li>ASCII_HEADER_ASCII_BODY</li>
	 * <li>BLANK_HEADER_BLANK_BODY</li>
	 * <li>SINGLE_BLANK_HEADER_SINGLE_BLANK_BODY</li>
	 * <li>ASCII_BLANK_HEADER_ASCII_BLANK_BODY</li>
	 * </ul>
	 * 
	 * @param aTableStyle The style to use for the logger.
	 */
	public void setStyle( String aTableStyle ) {
		setLoggerStyle( aTableStyle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TableStyle getTableStyle() {
		return _tableStyle;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTableStyle( TableStyle aTableStyle ) {
		_tableStyle = aTableStyle;
		init();
	}

	/**
	 * As the underlying configuration framework used by the
	 * {@link RuntimeLoggerSingleton} cannot convert a String to an enum, we got
	 * to provide such a method ourselves. When configuring programmatically use
	 * {@link #setTableStyle(TableStyle)}, when using the
	 * "<code>runtimelogger-config.xml</code>" use the property "loggerStyle"
	 * for this method to be invoked. Valid arguments are as of the
	 * {@link TableStyle} enumeration:
	 * <ul>
	 * <li>SINGLE_HEADER_SINGLE_BODY</li>
	 * <li>DOUBLE_SINGLE_HEADER_SINGLE_BODY</li>
	 * <li>DOUBLE_HEADER_DOUBLE_BODY</li>
	 * <li>DOUBLE_SINGLE_HEADER_DOUBLE_SINGLE_BODY</li>
	 * <li>DOUBLE_HEADER_SINGLE_BODY</li>
	 * <li>DOUBLE_HEADER_DOUBLE_SINGLE_BODY</li>
	 * <li>DOUBLE_SINGLE_HEADER_SINGLE_DASHED_BODY</li>
	 * <li>ASCII_HEADER_ASCII_BODY</li>
	 * <li>BLANK_HEADER_BLANK_BODY</li>
	 * <li>SINGLE_BLANK_HEADER_SINGLE_BLANK_BODY</li>
	 * <li>ASCII_BLANK_HEADER_ASCII_BLANK_BODY</li>
	 * </ul>
	 * 
	 * @param aTableStyleName The style to use for the logger.
	 */
	public void setLoggerStyle( String aTableStyleName ) {
		setTableStyle( TableStyle.valueOf( aTableStyleName ) );
	}

	/**
	 * Sets the escape codes.
	 *
	 * @param isEscCodesEnabled the new escape codes
	 */
	public void setEscapeCodes( boolean isEscCodesEnabled ) {
		_isEscCodesEnabled = isEscCodesEnabled;
		final TableBuilder theTableBuilder = _tableBuilder;
		if ( theTableBuilder != null ) {
			theTableBuilder.withEscapeCodesEnabled( isEscCodesEnabled );
		}
	}

	/**
	 * Checks for left border.
	 *
	 * @return true, if successful
	 */
	public boolean hasLeftBorder() {
		return _hasLeftBorder;
	}

	/**
	 * Sets the left border.
	 *
	 * @param hasLeftBorder the new left border
	 */
	public void setLeftBorder( boolean hasLeftBorder ) {
		_hasLeftBorder = hasLeftBorder;
		init();
	}

	/**
	 * With left border.
	 *
	 * @param hasLeftBorder the has left border
	 * 
	 * @return the formatted logger
	 */
	public AbstractConsoleLogger<T> withLeftBorder( boolean hasLeftBorder ) {
		setLeftBorder( hasLeftBorder );
		return this;
	}

	/**
	 * Checks for right border.
	 *
	 * @return true, if successful
	 */
	public boolean hasRightBorder() {
		return _hasRightBorder;
	}

	/**
	 * Sets the right border.
	 *
	 * @param hasRightBorder the new right border
	 */
	public void setRightBorder( boolean hasRightBorder ) {
		_hasRightBorder = hasRightBorder;
		init();
	}

	/**
	 * With right border.
	 *
	 * @param hasRightBorder the has right border
	 * 
	 * @return the formatted logger
	 */
	public AbstractConsoleLogger<T> withRightBorder( boolean hasRightBorder ) {
		setRightBorder( hasRightBorder );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AbstractConsoleLogger<T> withErrorPrintStream( PrintStream aErrorPrintStream ) {
		setErrorPrintStream( aErrorPrintStream );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AbstractConsoleLogger<T> withStandardPrintStream( PrintStream aStandardPrintStream ) {
		setStandardPrintStream( aStandardPrintStream );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AbstractConsoleLogger<?> withTableStyle( TableStyle aTableStyle ) {
		setTableStyle( aTableStyle );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AbstractConsoleLogger<?> withRowWidth( int aRowWidth ) {
		setRowWidth( aRowWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRowWidth() {
		return _rowWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowWidth( int aRowWidth ) {
		_rowWidth = aRowWidth;
		init();
	}

	/**
	 * Checks for escape codes.
	 *
	 * @return true, if successful
	 */
	public boolean hasEscapeCodes() {
		return _isEscCodesEnabled;
	}

	/**
	 * With escape codes.
	 *
	 * @param isEscCodesEnabled the is esc codes enabled
	 * 
	 * @return the formatted logger
	 */
	public AbstractConsoleLogger<T> withEscapeCodes( boolean isEscCodesEnabled ) {
		setEscapeCodes( isEscCodesEnabled );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		printTail();
	}

	/**
	 * Initializes the logger.
	 */
	protected void init() {
		final TableBuilder theTableBuilder = toPreConfiguredTableBuilder( _header, OutputPrintStream.STANDARD );
		_columnNames = new ArrayList<>();
		for ( ColumnSetupMetrics eMetrics : _header ) {
			if ( eMetrics.isVisible() ) {
				_columnNames.add( eMetrics.getName() );
			}
		}
		out: {
			for ( String eName : _columnNames ) {
				if ( eName != null && eName.length() != 0 ) {
					break out;
				}
			}
			_columnNames.clear();
			_columnNames = null;
		}

		_tableBuilder = theTableBuilder;
	}

	// /////////////////////////////////////////////////////////////////////////
	// DECORATOR:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printSeparator() {
		_isPrintSeparator = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printTail() {
		if ( _hasLogLines ) {
			_tableBuilder.printTail();
			_stdStream.flush();
			_hasLogLines = false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printHead() {
		_isPrintHead = true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a preconfigured {@link TableBuilder}, can be used by sub-classes
	 * in case them require additional {@link TableBuilder} instances.
	 *
	 * @param aHeader the header
	 * @param aOutputPrintStream the output print stream
	 * 
	 * @return A preconfigured {@link TableBuilder} (as of the attributes state
	 *         of the {@link AbstractConsoleLogger}).
	 */
	protected TableBuilder toPreConfiguredTableBuilder( FormattedHeader<?> aHeader, OutputPrintStream aOutputPrintStream ) {
		final TableBuilder theTableBuilder = new TableBuilder( _rowWidth ).withTableStyle( _tableStyle ).withPrintStream( aOutputPrintStream == OutputPrintStream.STANDARD ? _stdStream : _errStream ).withResetEscapeCode( _header.getResetEscapeCode() ).withLeftBorder( _hasLeftBorder ).withRightBorder( _hasRightBorder ).withEscapeCodesEnabled( _isEscCodesEnabled ).withLineBreak( Terminal.toLineBreak( _rowWidth ) );
		theTableBuilder.withBorderEscapeCode( DEFAULT_ANSI_BOX_GRID_COLOR );
		for ( ColumnSetupMetrics eMetrics : aHeader ) {
			if ( eMetrics.isVisible() ) {
				theTableBuilder.addColumn().withColumnFormatMetrics( eMetrics );
			}
		}
		return theTableBuilder;
	}

	/**
	 * Prints out a log-line with regard to the provided {@link Header} unsing
	 * the provided {@link TableBuilder}.
	 *
	 * @param aRecord The record to log.
	 * @param aHeader The {@link Header} with which to determine the visibility
	 *        of the {@link Record}'s elements.
	 * @param aTableBuilder The {@link TableBuilder} to use for printing.
	 * 
	 * @throws ClassCastException the class cast exception
	 */
	protected synchronized void log( Record<? extends T> aRecord, FormattedHeader<T> aHeader, TableBuilder aTableBuilder ) {
		if ( _isPrintHead ) {
			if ( _columnNames != null ) {
				_tableBuilder.setTableStatus( TableStatus.NONE );
				_tableBuilder.printHeaderBegin();
				_tableBuilder.printHeaderContinue( _columnNames );
				_tableBuilder.printHeaderComplete();
			}
			_isPrintHead = false;
		}
		if ( _isPrintSeparator ) {
			_isPrintSeparator = false;
		}
		try {
			final Row<String> theRow = aHeader.toPrintableRow( aRecord );
			for ( int i = aHeader.size() - 1; i >= 0; i-- ) {
				if ( !aHeader.get( i ).isVisible() && theRow.size() > i ) {
					theRow.remove( i );
				}
			}
			for ( int i = 0; i < theRow.size(); i++ ) {
				if ( theRow.get( i ) == null ) {
					theRow.remove( i );
					theRow.add( i, Text.NULL_VALUE.getText() );
				}

			}
			aTableBuilder.printRowContinue( theRow );
		}
		catch ( ColumnMismatchException | ClassCastException | HeaderMismatchException e ) {
			throw new IllegalRecordRuntimeException( aRecord, e );
		}
		_hasLogLines = true;
	}
}
