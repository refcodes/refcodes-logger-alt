// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.console;

import java.io.PrintStream;

import org.refcodes.logger.ColumnLayout;
import org.refcodes.logger.Logger;
import org.refcodes.logger.LoggerInstantiationRuntimeException;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.runtime.Execution;
import org.refcodes.tabular.Record;
import org.refcodes.textual.TableStyle;

/**
 * This {@link ConsoleLoggerSingleton} provides a {@link ConsoleLogger}
 * singleton .
 */
public class ConsoleLoggerSingleton extends ConsoleLogger {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static final ConsoleLogger _singleton;

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	static {
		_singleton = new ConsoleLogger();
		try {
			final Thread theHook = new Thread( () -> {
				try {
					_singleton.destroy();
				}
				catch ( Exception ignore ) {}
			} );
			theHook.setDaemon( true );
			Execution.addShutdownHook( theHook );
		}
		catch ( Exception ignore ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructor for constructing a pseudo singleton; this constructor is
	 * public to provide means to some configuration frameworks (such as
	 * commons-configurations from Apache) for using this
	 * {@link ConsoleLoggerSingleton} even when not being able to access the
	 * {@link #getInstance()} method. The instance overhead for providing
	 * singleton behavior is taken into account.
	 */
	public ConsoleLoggerSingleton() {}

	// /////////////////////////////////////////////////////////////////////////
	// SINGLETON:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link ConsoleLoggerSingleton}.
	 * 
	 * @return The {@link RuntimeLogger} singleton's instance.
	 * 
	 * @throws LoggerInstantiationRuntimeException Thrown in case instantiating
	 *         a {@link Logger} ({@link RuntimeLogger}) failed
	 */
	public static ConsoleLogger getInstance() {
		return _singleton;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Record<? extends Object> aRecord ) {
		_singleton.log( aRecord );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream getStandardPrintStream() {
		return _singleton.getStandardPrintStream();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStandardPrintStream( PrintStream aOutStream ) {
		_singleton.setStandardPrintStream( aOutStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PrintStream getErrorPrintStream() {
		return _singleton.getErrorPrintStream();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setErrorPrintStream( PrintStream aErrStream ) {
		_singleton.setErrorPrintStream( aErrStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TableStyle getTableStyle() {
		return _singleton.getTableStyle();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTableStyle( TableStyle aTableStyle ) {
		_singleton.setTableStyle( aTableStyle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLoggerStyle( String aTableStyleName ) {
		_singleton.setLoggerStyle( aTableStyleName );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEscapeCodes( boolean isEscCodesEnabled ) {
		_singleton.setEscapeCodes( isEscCodesEnabled );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasLeftBorder() {
		return _singleton.hasLeftBorder();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLeftBorder( boolean hasLeftBorder ) {
		_singleton.setLeftBorder( hasLeftBorder );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasRightBorder() {
		return _singleton.hasRightBorder();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRightBorder( boolean hasRightBorder ) {
		_singleton.setRightBorder( hasRightBorder );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRowWidth() {
		return _singleton.getRowWidth();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRowWidth( int aRowWidth ) {
		_singleton.setRowWidth( aRowWidth );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printHead() {
		_singleton.printHead();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printSeparator() {
		_singleton.printSeparator();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printTail() {
		_singleton.printTail();
	}

	// @Override
	// public void setMemberType( MemberType aMemberType ) {
	// _consoleLoggerSingleton.setMemberType( aMemberType );
	// }

	// @Override
	// public void setMemberTypeName( String aMemberType ) {
	// _consoleLoggerSingleton.setMemberTypeName( aMemberType );
	// }

	// /////////////////////////////////////////////////////////////////////////
	// BUILDER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLoggerSingleton withStandardPrintStream( PrintStream aOutStream ) {
		_singleton.setStandardPrintStream( aOutStream );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLoggerSingleton withErrorPrintStream( PrintStream aErrStream ) {
		_singleton.setErrorPrintStream( aErrStream );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLoggerSingleton withTableStyle( TableStyle aTableStyle ) {
		_singleton.setTableStyle( aTableStyle );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLoggerSingleton withEscapeCodes( boolean isEscCodesEnabled ) {
		_singleton.setEscapeCodes( isEscCodesEnabled );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLoggerSingleton withLeftBorder( boolean hasLeftBorder ) {
		_singleton.setLeftBorder( hasLeftBorder );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLoggerSingleton withRightBorder( boolean hasRightBorder ) {
		_singleton.setRightBorder( hasRightBorder );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConsoleLoggerSingleton withRowWidth( int aRowWidth ) {
		_singleton.withRowWidth( aRowWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLoggerLayout( String aLoggerLayout ) {
		_singleton.setLoggerLayout( aLoggerLayout );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLayout( String aLoggerLayout ) {
		_singleton.setLoggerLayout( aLoggerLayout );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasEscapeCodes() {
		return _singleton.hasEscapeCodes();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ColumnLayout getColumnLayout() {
		return _singleton.getColumnLayout();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setColumnLayout( ColumnLayout aColumnLayout ) {
		_singleton.setColumnLayout( aColumnLayout );
	}
}
