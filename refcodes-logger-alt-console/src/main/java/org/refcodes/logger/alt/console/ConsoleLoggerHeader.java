// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.console;

import org.refcodes.data.AnsiEscapeCode;
import org.refcodes.logger.ColumnLayout;
import org.refcodes.logger.LoggerField;
import org.refcodes.tabular.FormattedColumnDecorator;
import org.refcodes.tabular.FormattedHeader;
import org.refcodes.tabular.PrintStackTrace;
import org.refcodes.textual.ColumnSetupMetrics;

/**
 * The Class ConsoleLoggerHeaderImpl.
 */
public class ConsoleLoggerHeader extends FormattedHeader<Object> {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String ANSI_RESET = AnsiEscapeCode.toEscapeSequence( AnsiEscapeCode.RESET ); // reset()

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new console logger header impl.
	 */
	public ConsoleLoggerHeader() {
		this( PrintStackTrace.EXPLODED, ColumnLayout.NONE.toColumnSetupMetrics() );
	}

	/**
	 * Instantiates a new console logger header impl.
	 *
	 * @param aLayout the layout
	 */
	public ConsoleLoggerHeader( ColumnLayout aLayout ) {
		this( PrintStackTrace.EXPLODED, aLayout != null ? aLayout.toColumnSetupMetrics() : ColumnLayout.NONE.toColumnSetupMetrics() );
	}

	/**
	 * Instantiates a new console logger header impl.
	 *
	 * @param aPrintStackTrace the print stack trace
	 * @param aColumnSetupMetrics the column setup metrics
	 */
	@SuppressWarnings("unchecked")
	public ConsoleLoggerHeader( PrintStackTrace aPrintStackTrace, ColumnSetupMetrics[] aColumnSetupMetrics ) {
		// @formatter:off
		super( 
			new FormattedColumnDecorator<>( LoggerField.LOG_LINE_NUMBER.getColumn(), aColumnSetupMetrics[0]),
			new FormattedColumnDecorator<>( LoggerField.LOG_DATE.getColumn(), aColumnSetupMetrics[1]),
			new FormattedColumnDecorator<>( LoggerField.LOG_PRIORITY.getColumn(), aColumnSetupMetrics[2]),
			new FormattedColumnDecorator<>( LoggerField.LOG_THREAD_NAME.getColumn(), aColumnSetupMetrics[3]),
			new FormattedColumnDecorator<>( LoggerField.LOG_SESSION_ID.getColumn(), aColumnSetupMetrics[4]),
			new FormattedColumnDecorator<>( LoggerField.LOG_REQUEST_ID.getColumn(), aColumnSetupMetrics[5]),
			new FormattedColumnDecorator<>( LoggerField.LOG_FULLY_QUALIFIED_CLASS_NAME.getColumn(), aColumnSetupMetrics[6]),
			new FormattedColumnDecorator<>( LoggerField.LOG_CLASS_LINE_NUMBER.getColumn(), aColumnSetupMetrics[7]),
			new FormattedColumnDecorator<>( LoggerField.LOG_METHODE_NAME.getColumn(), aColumnSetupMetrics[8]),
			new FormattedColumnDecorator<>( LoggerField.LOG_MESSAGE.getColumn(), aColumnSetupMetrics[9]),
			new FormattedColumnDecorator<>( LoggerField.toExceptionLoggerField( aPrintStackTrace ).getColumn(),  aColumnSetupMetrics[10])
		);
		// @formatter:on
		setResetEscapeCode( ANSI_RESET );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////
}
