// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/LICENSE-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.console;

import org.junit.jupiter.api.Test;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;

public class JulLoggerHandlerTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testJulLoggerHandler1() {
		final RuntimeLogger theRuntimeLogger = RuntimeLoggerFactorySingleton.createRuntimeLogger();
		theRuntimeLogger.info( "Hallo REFCODES Welt!" );
		final java.util.logging.Logger theJulLogger = java.util.logging.Logger.getLogger( getClass().getName() );
		theJulLogger.info( "Hallo JUL Welt!" );
	}

	@Test
	public void testJulLoggerHandler2() {
		RuntimeLoggerFactorySingleton.bindJavaUtilLogging();
		final java.util.logging.Logger theJulLogger = java.util.logging.Logger.getLogger( getClass().getName() );
		theJulLogger.info( "Hallo JUL Welt!" );
		final RuntimeLogger theRuntimeLogger = RuntimeLoggerFactorySingleton.createRuntimeLogger();
		theRuntimeLogger.info( "Hallo REFCODES Welt!" );
	}
}
