// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.console;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerImpl;
import org.refcodes.logger.RuntimeLoggerSingleton;
import org.refcodes.runtime.Execution;
import org.refcodes.textual.RandomTextGenerartor;
import org.refcodes.textual.RandomTextMode;

public class ConsoleRuntimeLoggerTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = LogManager.getLogger( ConsoleRuntimeLoggerTest.class );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled("Used for debugging only")
	@Test
	public void debugTest() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		RuntimeLogger theRuntimeLogger = RuntimeLoggerSingleton.getInstance();
		theRuntimeLogger.info( "Test1" );
		theRuntimeLogger.printSeparator();
		theRuntimeLogger.info( "Test2" );
		final ConsoleLogger theLogger = new ConsoleLogger();
		theRuntimeLogger = new RuntimeLoggerImpl( theLogger, LogPriority.TRACE );
		theRuntimeLogger.info( "Test1" );
		theRuntimeLogger.printSeparator();
		theRuntimeLogger.info( "Test2" );
	}

	@Test
	public void testConsoleRuntimeLogger() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		doReferenceLogger();
		final ConsoleLogger theLogger = new ConsoleLogger();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( theLogger, LogPriority.TRACE );
		doTestLogger( theRuntimeLogger );
	}

	@Test
	public void testPimpMyLogger() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RandomTextGenerartor theRnd20Generator = new RandomTextGenerartor().withColumnWidth( 20 ).withRandomTextMode( RandomTextMode.ALPHABETIC );
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		for ( int i = 0; i < 5; i++ ) {
			theRuntimeLogger.trace( theRnd20Generator.next() );
		}
		for ( int i = 0; i < 5; i++ ) {
			theRuntimeLogger.debug( theRnd20Generator.next() );
		}
		for ( int i = 0; i < 5; i++ ) {
			theRuntimeLogger.info( theRnd20Generator.next() );
		}

		// @formatter:off
		/*
		  	theRuntimeLogger.warn( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next(), new RuntimeException( theRnd20Generator.next() ) );
			theRuntimeLogger.warn( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next(), new RuntimeException( theRnd20Generator.next() ) );
			theRuntimeLogger.debug( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() );
			theRuntimeLogger.warn( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() );
			theRuntimeLogger.debug( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() );
			theRuntimeLogger.warn( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() );
			theRuntimeLogger.error( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next(), new RuntimeException( theRnd20Generator.next() ) );
			theRuntimeLogger.warn( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() );
			theRuntimeLogger.debug( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() );
			theRuntimeLogger.warn( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() );
			theRuntimeLogger.error( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next(), new RuntimeException( theRnd20Generator.next() ) );
		 	theRuntimeLogger.warn( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next(), new RuntimeException( theRnd20Generator.next() ) );
			theRuntimeLogger.error( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next(), new RuntimeException( theRnd20Generator.next() ) );
			theRuntimeLogger.debug( theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() + "-" + theRnd20Generator.next() );
			RandomTextGenerartor theRnd12Generator = new RandomTextGenerartorImpl().withColumnWidth( 12 ).withRandomTextMode( RandomTextMode.ALPHABETIC );
			for ( int i = 0; i < 5; i++ ) {
				theRuntimeLogger.warn( theRnd20Generator.next(), new RuntimeException( theRnd12Generator.next() ) );
			}
			for ( int i = 0; i < 5; i++ ) {
				theRuntimeLogger.error( theRnd20Generator.next(), new RuntimeException( theRnd12Generator.next() ) );
			}
			for ( int i = 0; i < 5; i++ ) {
				theRuntimeLogger.panic( theRnd20Generator.next(), new RuntimeException( theRnd12Generator.next() ) );
			}
			for ( int i = 0; i < 5; i++ ) {
				theRuntimeLogger.debug( theRnd20Generator.next() );
			}
		*/
		// @formatter:on
	}

	@Test
	public void testLogLevelMix1() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.warn( "Warn" );
		theRuntimeLogger.info( "Info" );
	}

	@Test
	public void testLogLevelMix2() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.info( "Info" );
		theRuntimeLogger.warn( "Warn" );
	}

	@Test
	public void testLogLevelMix3() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.info( "Info" );
		theRuntimeLogger.error( "Warn", new RuntimeException( "Surprise!" ) );
	}

	@Test
	public void testLogLevelMix4() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.warn( "Warn", new RuntimeException( "Surprise!" ) );
		theRuntimeLogger.error( "Info" );
	}

	@Test
	public void testLogLevelMix5() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.info( "Info" );
		theRuntimeLogger.warn( "Warn" );
		theRuntimeLogger.printTail();
		System.out.println( "Hello world!" );
		theRuntimeLogger.printHead();
		theRuntimeLogger.info( "Info" );
	}

	@Test
	public void testLogLevelMix6() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.info( "Info" );
		theRuntimeLogger.warn( "Warn" );
		theRuntimeLogger.printTail();
		System.out.println( "Hello world!" );
		theRuntimeLogger.printHead();
		theRuntimeLogger.error( "Error", new RuntimeException( "Surprise!" ) );
	}

	@Test
	public void testLogLevelMix7() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.info( "Info" );
		theRuntimeLogger.error( "Error", new RuntimeException( "Surprise!" ) );
		theRuntimeLogger.printTail();
		System.out.println( "Hello world!" );
		theRuntimeLogger.printHead();
		theRuntimeLogger.error( "Error", new RuntimeException( "Surprise!" ) );
	}

	@Test
	public void testLogLevelMix8() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.info( "Info" );
		theRuntimeLogger.error( "Error", new RuntimeException( "Surprise!" ) );
		theRuntimeLogger.printTail();
		System.out.println( "Hello world!" );
		theRuntimeLogger.error( "Error", new RuntimeException( "Surprise!" ) );
	}

	@Test
	public void testLogLevelMix9() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.info( "Info" );
		theRuntimeLogger.error( "Error", new RuntimeException( "Surprise!" ) );
		theRuntimeLogger.printTail();
		theRuntimeLogger.printTail();
		theRuntimeLogger.printTail();
		theRuntimeLogger.printTail();
		System.out.println( "Hello world!" );
		theRuntimeLogger.printTail();
		theRuntimeLogger.printTail();
		theRuntimeLogger.printTail();
		theRuntimeLogger.printTail();
		theRuntimeLogger.error( "Error", new RuntimeException( "Surprise!" ) );
	}

	@Test
	public void testLogLevelMix10() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.info( "Info" );
		theRuntimeLogger.printTail();
		theRuntimeLogger.printTail();
		theRuntimeLogger.printTail();
		theRuntimeLogger.printTail();
		System.out.println( "Hello world!" );
		theRuntimeLogger.info( "Info" );
	}

	@Test
	public void testEdgeCase() {
		System.out.println();
		System.out.println( "*** " + Execution.getCallerStackTraceElement().getMethodName() + " ***" );
		System.out.println();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( new ConsoleLogger(), LogPriority.TRACE );
		theRuntimeLogger.warn( "Test1" );
		theRuntimeLogger.printTail();
		try {
			Thread.sleep( 1500 );
		}
		catch ( Exception e ) {}
		System.out.println( "HELL0 W0RLD!" );
		theRuntimeLogger.printHead();
		theRuntimeLogger.info( "Test1" );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void doTestLogger( RuntimeLogger aRuntimeLogger ) {
		LOGGER.info( "------------------------------ Runtime logger log ------------------------------" );
		aRuntimeLogger.trace( "Hallo Straße!" );
		aRuntimeLogger.debug( "Hallo Stadt!" );
		aRuntimeLogger.info( "Hallo Mond!" );
		// aRuntimeLogger.warn( "Hallo Mars!", new RuntimeException( "Mars
		// exception!" ) );
		// aRuntimeLogger.error( "Hallo Saturn!", new RuntimeException( "Saturn
		// exception!" ) );
		// aRuntimeLogger.panic( "Hallo Milchstraße!", new RuntimeException(
		// "Milchstraße exception!" ) );
		System.out.flush();
		System.err.flush();
	}

	private void doReferenceLogger() {
		LOGGER.info( "----------------------------- Reference Log4j log ------------------------------" );
		final Logger theLog4jLogger = LogManager.getLogger( getClass() );
		theLog4jLogger.trace( "Hallo Straße!" );
		theLog4jLogger.debug( "Hallo Stadt!" );
		theLog4jLogger.info( "Hallo Mond!" );
		// theLog4jLogger.warn( "Hallo Mars!", new RuntimeException( "Mars
		// exception!" ) );
		// theLog4jLogger.error( "Hallo Saturn!", new RuntimeException( "Saturn
		// exception!" ) );
		// theLog4jLogger.fatal( "Hallo Milchstraße!", new RuntimeException(
		// "Milchstraße exception!" ) );
		System.out.flush();
		System.err.flush();
	}
}
