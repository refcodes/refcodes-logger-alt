// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.async;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.refcodes.component.Destroyable;
import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.controlflow.RetryCounter;
import org.refcodes.data.IoRetryCount;
import org.refcodes.data.RetryCount;
import org.refcodes.data.SleepLoopTime;
import org.refcodes.exception.Trap;
import org.refcodes.logger.IllegalRecordRuntimeException;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.Logger;
import org.refcodes.logger.LoggerAccessor.LoggerMutator;
import org.refcodes.logger.UnexpectedLogRuntimeException;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.RecordImpl;
import org.refcodes.tabular.Records;

/**
 * The {@link AsyncLogger} uses the asynchronous patter to forward
 * {@link Logger} functionality to an encapsulated {@link Logger} instance.
 * <p>
 * Internally a log line queue (holding {@link Record} instances to be logged)
 * as well a daemon thread (taking elements from the log line queue) are used to
 * decouple the encapsulated {@link Logger} instance from the asynchronous
 * {@link AsyncLogger}.
 * <p>
 * A given number of retries are approached in case there is an overflow of the
 * log line queue; this happens when the queue is full the encapsulated
 * {@link Logger} instance cannot take the next {@link Record} in time.
 * <p>
 * To avoid a building up of the log line queue, eventually causing an out of
 * memory, log lines not being taken into the log line queue within the given
 * number of retries, them log lines are dismissed. In such a case a warning
 * with a {@link LogPriority#WARN} is printed out.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public class AsyncLogger<T> implements Logger<T>, Destroyable, LoggerMutator<Logger<T>> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger( AsyncLogger.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int LOGGERS_MULTIPLIER = 1000;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private LinkedBlockingQueue<Record<? extends T>> _logLineQueue;
	private boolean _isDestroyed = false;
	private boolean _isInitialized = false;
	private final SeparatorRecord<T> _separator = new SeparatorRecord<>();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new async logger.
	 */
	public AsyncLogger() {}

	/**
	 * Constructs an asynchronous {@link Logger} ({@link AsyncLogger}) from the
	 * provided {@link Logger} instance.
	 *
	 * @param aLogger The a logger which is to be enriched with asynchronous
	 *        functionality.
	 */
	public AsyncLogger( Logger<T> aLogger ) {
		init( ControlFlowUtility.createCachedExecutorService( true ), aLogger );
	}

	/**
	 * Constructs an {@link AsyncLogger} from the provided {@link Logger}
	 * instance.
	 * 
	 * @param aExecutorService The {@link ExecutorService} to use when creating
	 *        threads.
	 * @param aLogger The {@link Logger} instances to be used for the
	 *        {@link AsyncLogger}.
	 */
	public AsyncLogger( ExecutorService aExecutorService, Logger<T> aLogger ) {
		init( aExecutorService, aLogger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Record<? extends T> aRecord ) {
		// ---------------------------------------------------------------------
		// We assume to log to physical data sinks, therefore we use IO retries.
		// ---------------------------------------------------------------------
		final RetryCounter theRetryCounter = new RetryCounter( IoRetryCount.MAX.getValue() );
		try {
			while ( !_logLineQueue.offer( aRecord, SleepLoopTime.MAX.getTimeMillis(), TimeUnit.MILLISECONDS ) && theRetryCounter.nextRetry() ) {
				LOGGER.log( Level.WARNING, "Trying to offer (add) a log line to the log line queue, though the queue is full, this is retry # <" + theRetryCounter.getRetryCount() + ">, aborting after <" + theRetryCounter.getRetryNumber() + "> retries. Retrying now after a delay of <" + ( SleepLoopTime.MAX.getTimeMillis() / 1000 ) + "> seconds..." );
				if ( !theRetryCounter.hasNextRetry() ) {
					throw new UnexpectedLogRuntimeException( "Unable to process the log line after <" + theRetryCounter.getRetryNumber() + "> retries, aborting retries, dismissing log line \"" + aRecord.toString() + "\"!", aRecord );
				}
			}
		}
		catch ( InterruptedException ignored ) {}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void printSeparator() {
		log( _separator );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		_isDestroyed = true;
		final RetryCounter theRetryCounter = new RetryCounter( RetryCount.MAX.getValue(), SleepLoopTime.NORM.getTimeMillis() );
		while ( theRetryCounter.hasNextRetry() && !_logLineQueue.isEmpty() ) {
			LOGGER.log( Level.WARNING, "The logline queue is not empty, waiting <" + theRetryCounter.getNextRetryDelayMillis() + "> ms for next retry number <" + theRetryCounter.getRetryCount() + "> (of <" + theRetryCounter.getRetryNumber() + "> altogether)." );
			theRetryCounter.nextRetry();
		}
		final int theLogLineQueueSize = _logLineQueue.size();
		if ( theLogLineQueueSize != 0 ) {
			LOGGER.log( Level.WARNING, "The logline queue was not empty (with size <" + theLogLineQueueSize + ">) upon destroying this component." );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Inits the.
	 *
	 * @param aExecutorService the executor service
	 * @param aLogger the logger
	 */
	private void init( ExecutorService aExecutorService, Logger<T> aLogger ) {
		if ( _isInitialized ) {
			throw new IllegalStateException( "Unable re-initialize the asynchronous logger after it already has been initialized with a eunning logger!" );
		}
		if ( aLogger == null ) {
			throw new IllegalArgumentException( "Unable to construct the asynchronous logger as there must at least one logger instance provided!" );
		}
		_logLineQueue = new LinkedBlockingQueue<>( LOGGERS_MULTIPLIER );
		final LogDaemon theDaemon = new LogDaemon( aLogger );
		aExecutorService.execute( theDaemon );
		_isInitialized = true;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setLogger( Logger<T> aLogger ) {
		init( ControlFlowUtility.createCachedExecutorService( true ), aLogger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// DEAMONS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The LogDaemon is a daemon thread taking log line {@link Record} instances
	 * from the log line queue to pass them to a dedicated {@link Logger}
	 * instance. Many LogDaemon threads operate on the same log line queue
	 * feeding the log line {@link Records} instances each to a dedicated
	 * {@link Logger} instance in parallel.
	 */
	private class LogDaemon extends Thread {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final Logger<T> _logger;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Constructs LogDeaon responsible for feeding log {@link Record}
		 * instances from a log line queue to its encapsulated {@link Logger}..
		 * 
		 * @param aLogger The logger fed with log {@link Record} instances from
		 *        the log line queue.
		 */
		public LogDaemon( Logger<T> aLogger ) {
			_logger = aLogger;
			setDaemon( true );
			setPriority( NORM_PRIORITY );
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void run() {
			Record<? extends T> eRecord = null;
			while ( !_logLineQueue.isEmpty() || !_isDestroyed ) {
				try {
					eRecord = _logLineQueue.take();
					if ( eRecord == _separator ) {
						_logger.printSeparator();
					}
					else {
						_logger.log( eRecord );
					}
				}
				catch ( InterruptedException e ) {
					break;
				}
				catch ( IllegalRecordRuntimeException e ) {
					LOGGER.log( Level.WARNING, "As of an illegal record, the daemon is unable to log the log line \"" + eRecord + "\" as of: " + Trap.asMessage( e ), e );
				}
				catch ( UnexpectedLogRuntimeException e ) {
					LOGGER.log( Level.WARNING, "As of an unexpected log exception, the daemon is unable to log the log line \"" + eRecord + "\" as of: " + Trap.asMessage( e ), e );
				}
				catch ( Exception e ) {
					LOGGER.log( Level.WARNING, "As of an unrecognized exception, the daemon is unable to log the log line \"" + eRecord + "\" as of: " + Trap.asMessage( e ), e );
				}
			}
		}
	}

	@SuppressWarnings({ "serial" })
	private static class SeparatorRecord<T> extends RecordImpl<T> {}
}
