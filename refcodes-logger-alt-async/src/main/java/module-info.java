module org.refcodes.logger.alt.async {
	requires org.refcodes.controlflow;
	requires org.refcodes.data;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.logger;
	requires transitive org.refcodes.tabular;

	exports org.refcodes.logger.alt.async;
}
