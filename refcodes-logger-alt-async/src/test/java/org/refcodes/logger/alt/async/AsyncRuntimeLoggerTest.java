// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.async;

import org.junit.jupiter.api.Test;
import org.refcodes.data.SleepLoopTime;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerImpl;
import org.refcodes.logger.SystemLogger;

/**
 * The Class AsyncRuntimeLoggerTest.
 */
public class AsyncRuntimeLoggerTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testAsyncRuntimeLoggerImpl() throws InterruptedException {
		final SystemLogger theSystemLogger = new SystemLogger();
		final AsyncLogger<Object> theLogger = new AsyncLogger<>( theSystemLogger );
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( theLogger, LogPriority.TRACE );
		doTestLogger( theRuntimeLogger );
		// ---------------------------------------------------------------------
		// As we are doing it asynchronously and the thread picking up our logs
		// would die with the current thread and not be able to print the logs:
		// ---------------------------------------------------------------------
		Thread.sleep( SleepLoopTime.MIN.getTimeMillis() );
		// ---------------------------------------------------------------------
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void doTestLogger( RuntimeLogger aRuntimeLogger ) {
		aRuntimeLogger.trace( "Hallo Straße!" );
		aRuntimeLogger.debug( "Hallo Stadt!" );
		aRuntimeLogger.info( "Hallo Mond!" );
		// aRuntimeLogger.warn( "Hallo Mars!", new RuntimeException( "Mars exception!" ) );
		// aRuntimeLogger.error( "Hallo Saturn!", new RuntimeException( "Saturn exception!" ) );
		// aRuntimeLogger.panic( "Hallo Milchstraße!", new RuntimeException( "Milchstraße exception!" ) );
		System.out.flush();
		System.err.flush();
	}
}
