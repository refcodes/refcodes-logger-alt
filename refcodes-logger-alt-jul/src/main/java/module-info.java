module org.refcodes.logger.alt.jul {
	requires transitive org.refcodes.logger;
	requires transitive org.refcodes.mixin;
	requires org.refcodes.runtime;

	exports org.refcodes.logger.alt.jul;
}
