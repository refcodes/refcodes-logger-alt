// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.jul;

import java.util.logging.LogRecord;

import org.refcodes.logger.IllegalRecordRuntimeException;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.Logger;
import org.refcodes.logger.LoggerField;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerImpl;
import org.refcodes.logger.RuntimeLoggerSingleton;
import org.refcodes.mixin.NameAccessor.NameMutator;
import org.refcodes.runtime.Execution;
import org.refcodes.tabular.ColumnMismatchException;
import org.refcodes.tabular.Record;

/**
 * The {@link JulLogger} implements the {@link Logger} interface for providing
 * logging functionality using the {@link java.util.logging.Logger} framework.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public class JulLogger<T> implements Logger<T>, NameMutator {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private transient java.util.logging.Logger _logger;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link JulLogger}.
	 */
	public JulLogger() {
		StackTraceElement theCaller = Execution.probeTillNoneLoggerElement( RuntimeLoggerImpl.class, RuntimeLogger.class, RuntimeLoggerSingleton.class, JulLogger.class );
		_logger = java.util.logging.Logger.getLogger( theCaller.getClassName() );
	}

	/**
	 * Instantiates a new {@link JulLogger} by setting the underlying
	 * {@link java.util.logging.Logger}'s name with the provided name. See also
	 * {@link #setName(String)}.
	 *
	 * @param aName The name to be used for the logger.
	 */
	public JulLogger( String aName ) {
		_logger = java.util.logging.Logger.getLogger( aName );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Record<? extends T> aRecord ) {
		try {
			final LogPriority thePriority = (LogPriority) LoggerField.LOG_PRIORITY.getColumn().get( aRecord );
			final String theClassName = (String) LoggerField.LOG_FULLY_QUALIFIED_CLASS_NAME.getColumn().get( aRecord );
			final String theMethodName = (String) LoggerField.LOG_METHODE_NAME.getColumn().get( aRecord );
			final String theLogMessage = (String) LoggerField.LOG_MESSAGE.getColumn().get( aRecord );
			final Exception theException = (Exception) LoggerField.LOG_NONE_STACKTRRACE_EXCEPTION.getColumn().get( aRecord );
			final LogRecord theRecord = new LogRecord( thePriority.toLevel(), theLogMessage );
			theRecord.setSourceClassName( theClassName );
			theRecord.setSourceMethodName( theMethodName );
			theRecord.setThrown( theException );

			final java.util.logging.Logger theLogger = theClassName != null ? java.util.logging.Logger.getLogger( theClassName ) : _logger;
			theLogger.log( theRecord );
		}
		catch ( ColumnMismatchException | ClassCastException e ) {
			throw new IllegalRecordRuntimeException( aRecord, e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setName( String aName ) {
		_logger = java.util.logging.Logger.getLogger( aName );
	}
}
