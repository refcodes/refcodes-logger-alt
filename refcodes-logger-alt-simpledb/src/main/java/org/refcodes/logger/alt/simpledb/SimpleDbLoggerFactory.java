// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.simpledb;

import org.refcodes.logger.Logger;
import org.refcodes.logger.LoggerFactory;
import org.refcodes.logger.LoggerUtility;
import org.refcodes.tabular.ColumnFactory;

/**
 * Ready to use {@link LoggerFactory} to create {@link Logger} instances
 * directly attached to Amazon SimpleDb.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public class SimpleDbLoggerFactory<T> extends AbstractSimpleDbLoggerFactory<Logger<T>, T> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new simple db logger factory impl.
	 *
	 * @param aDomainPrefix the domain prefix
	 * @param aAccessKey the access key
	 * @param aSecretKey the secret key
	 * @param aEndPoint the end point
	 * @param aColumnFactory the column factory
	 */
	public SimpleDbLoggerFactory( String aDomainPrefix, String aAccessKey, String aSecretKey, String aEndPoint, ColumnFactory<T> aColumnFactory ) {
		super( aDomainPrefix, aAccessKey, aSecretKey, aEndPoint, aColumnFactory );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Logger<T> create( String aSchemaSuffix ) {
		return new SimpleDbLogger<>( LoggerUtility.getSchemaName( getSchemaPrefix(), aSchemaSuffix ), getAccessKey(), getSecretKey(), getEndPoint(), getColumnFactory() );
	}
}
