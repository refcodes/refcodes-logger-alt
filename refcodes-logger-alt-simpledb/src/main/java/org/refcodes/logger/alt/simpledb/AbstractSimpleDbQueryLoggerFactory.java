// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.simpledb;

import org.refcodes.logger.Logger;
import org.refcodes.logger.LoggerFactory;
import org.refcodes.logger.QueryLogger;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.ColumnFactory;

/**
 * The {@link AbstractSimpleDbQueryLoggerFactory} implements the
 * {@link LoggerFactory} and creates {@link QueryLogger} instances using the
 * SimpleDB NoSQL engine from Amazon. The {@link LoggerFactory} methods
 * {@link #create(String)} and {@link #create(String)} actually return
 * {@link TrimLoggerSimpleDbImpl} instances being configured to use an Amazon
 * SimpleDB domain name constructed from an Amazon SimpleDB domain prefix
 * (provided to the constructor) and suffix (provided to the factory methods).
 *
 * @param <L> The type (sub-type) of the {@link Logger} to be created.
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
abstract class AbstractSimpleDbQueryLoggerFactory<L extends QueryLogger<T>, T> extends AbstractSimpleDbLoggerFactory<L, T> implements LoggerFactory<L> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an {@link AbstractSimpleDbQueryLoggerFactory} for creating Amazon
	 * SimpleDB based {@link QueryLogger} instances.
	 *
	 * @param aDomainPrefix The Amazon SimpleDB domain name's prefix.
	 * @param aAccessKey The Amazon access key to use.
	 * @param aSecretKey The Amazon secret key to use.
	 * @param aEndPoint The end-point (Amazon region) to use.
	 * @param aColumnFactory The {@link ColumnFactory} used to handle the
	 *        {@link Column} instances.
	 */
	public AbstractSimpleDbQueryLoggerFactory( String aDomainPrefix, String aAccessKey, String aSecretKey, String aEndPoint, ColumnFactory<T> aColumnFactory ) {
		super( aDomainPrefix, aAccessKey, aSecretKey, aEndPoint, aColumnFactory );
	}
}
