// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.simpledb;

import java.util.Map;
import java.util.logging.Level;

import org.refcodes.logger.Logger;
import org.refcodes.logger.LoggerFactory;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.ColumnFactory;
import org.refcodes.textual.SecretHintBuilder;

/**
 * The {@link AbstractSimpleDbLoggerFactory} implements the
 * {@link LoggerFactory} and creates {@link Logger} instances using the SimpleDB
 * NoSQL engine from Amazon. The {@link LoggerFactory} methods
 * {@link #create(String)} and {@link #create(String)} actually return
 * {@link SimpleDbLogger} instances being configured to use an Amazon SimpleDB
 * domain name constructed from an Amazon SimpleDB domain prefix (provided to
 * the constructor) and suffix (provided to the factory methods).
 *
 * @param <L> The type (sub-type) of the {@link Logger} to be created.
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
abstract class AbstractSimpleDbLoggerFactory<L extends Logger<T>, T> implements LoggerFactory<L> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger( AbstractSimpleDbLoggerFactory.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final String _schemaPrefix;
	private final String _accessKey;
	private final String _secretKey;
	private final String _endPoint;
	private final ColumnFactory<T> _columnFactory;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an {@link AbstractSimpleDbLoggerFactory} for creating Amazon
	 * SimpleDB based {@link Logger} instances.
	 * 
	 * @param aDomainPrefix The Amazon SimpleDB domain name's prefix.
	 * @param aAccessKey The Amazon access key to use.
	 * @param aSecretKey The Amazon secret key to use.
	 * @param aEndPoint The end-point (Amazon region) to use.
	 * @param aColumnFactory The {@link ColumnFactory} used to handle the
	 *        {@link Column} instances.
	 */
	public AbstractSimpleDbLoggerFactory( String aDomainPrefix, String aAccessKey, String aSecretKey, String aEndPoint, ColumnFactory<T> aColumnFactory ) {
		_schemaPrefix = aDomainPrefix;
		_accessKey = aAccessKey;
		_secretKey = aSecretKey;
		_endPoint = aEndPoint;
		_columnFactory = aColumnFactory;
		LOGGER.log( Level.FINE, "Using Amazon SimpleDB domain prefix \"" + aDomainPrefix + "\", access key \"" + SecretHintBuilder.asString( aAccessKey ) + "\", secret key \"" + SecretHintBuilder.asString( aSecretKey ) + "\" and the end-point \"" + aEndPoint + "\"." );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public abstract L create( String aDomainSuffix );

	/**
	 * {@inheritDoc}
	 */
	@Override
	public L create( String aDomainSuffix, Map<String, String> aProperties ) {
		return create( aDomainSuffix );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Provides access to the schema prefix.
	 * 
	 * @return The schema prefix
	 */
	protected String getSchemaPrefix() {
		return _schemaPrefix;
	}

	/**
	 * Retrieves the Amazon access key.
	 * 
	 * @return The Amazon access key.
	 */
	protected String getAccessKey() {
		return _accessKey;
	}

	/**
	 * Retrieves the Amazon secret key.
	 * 
	 * @return The Amazon secret key.
	 */
	protected String getSecretKey() {
		return _secretKey;
	}

	/**
	 * Retrieves the Amazon SimpleDB end-point.
	 * 
	 * @return The Amazon SimpleDB end-point.
	 */
	protected String getEndPoint() {
		return _endPoint;
	}

	/**
	 * Retrieves the {@link ColumnFactory} to be used.
	 * 
	 * @return The {@link ColumnFactory} to be used.
	 */
	protected ColumnFactory<T> getColumnFactory() {
		return _columnFactory;
	}
}
