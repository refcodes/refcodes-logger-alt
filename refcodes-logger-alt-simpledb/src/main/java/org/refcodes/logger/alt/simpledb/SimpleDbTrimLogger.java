// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.simpledb;

import org.refcodes.component.Component;
import org.refcodes.component.Decomposable;
import org.refcodes.component.Flushable;
import org.refcodes.component.Initializable;
import org.refcodes.criteria.Criteria;
import org.refcodes.logger.Logger;
import org.refcodes.logger.TrimLogger;
import org.refcodes.tabular.ColumnFactory;

import com.amazonaws.services.simpledb.model.DeleteAttributesRequest;

/**
 * The {@link SimpleDbTrimLogger} extends the {@link SimpleDbQueryLogger}
 * implementation with the {@link TrimLogger} functionality.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public class SimpleDbTrimLogger<T> extends SimpleDbQueryLogger<T> implements TrimLogger<T>, Component, Initializable, Decomposable, Flushable {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new simple db trim logger impl.
	 *
	 * @param aDomainName the domain name
	 * @param aAccessKey the access key
	 * @param aSecretKey the secret key
	 * @param aColumnFactory the column factory
	 */
	public SimpleDbTrimLogger( String aDomainName, String aAccessKey, String aSecretKey, ColumnFactory<T> aColumnFactory ) {
		super( aDomainName, aAccessKey, aSecretKey, null, aColumnFactory );
	}

	/**
	 * Instantiates a new simple db trim logger impl.
	 *
	 * @param aDomainName the domain name
	 * @param aAccessKey the access key
	 * @param aSecretKey the secret key
	 * @param aEndPoint the end point
	 * @param aColumnFactory the column factory
	 */
	public SimpleDbTrimLogger( String aDomainName, String aAccessKey, String aSecretKey, String aEndPoint, ColumnFactory<T> aColumnFactory ) {
		super( aDomainName, aAccessKey, aSecretKey, aEndPoint, aColumnFactory );

	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteLogs( Criteria aCriteria ) {
		flushBuffer();
		// ---------------------------------------------------------------------
		// ???: There might be a more direct way of deleting from a SimpleDB:
		// ---------------------------------------------------------------------
		final String theSelectQuery = toSqlQuery( aCriteria, -1 );
		final SimpleDbRecords e = new SimpleDbRecords( theSelectQuery );
		DeleteAttributesRequest eDeleteAttributesRequest;
		while ( e.hasNext() ) {
			eDeleteAttributesRequest = new DeleteAttributesRequest( getAmazonSimpleDbDomainName(), e.nextItem().getName() );
			getAmazonSimpleDbClient().deleteAttributes( eDeleteAttributesRequest );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		clearDomain( getAmazonSimpleDbClient(), getAmazonSimpleDbDomainName() );
	}
}
