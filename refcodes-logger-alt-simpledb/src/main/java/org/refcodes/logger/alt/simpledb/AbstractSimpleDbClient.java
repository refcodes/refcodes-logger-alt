// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.simpledb;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.refcodes.exception.Trap;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.AmazonSimpleDBClientBuilder;
import com.amazonaws.services.simpledb.model.CreateDomainRequest;
import com.amazonaws.services.simpledb.model.DeleteDomainRequest;
import com.amazonaws.services.simpledb.model.DomainMetadataRequest;
import com.amazonaws.services.simpledb.model.ListDomainsResult;
import com.amazonaws.services.simpledb.model.NoSuchDomainException;
import com.amazonaws.services.simpledb.model.RequestTimeoutException;

//@formatter:off
/**
 * Abstract class to be used for any SimpleDB using service. An endpoint can be
 * specified (i.e. the location where Amazon SimpleDB will store the data):
 *
 * Possible endpoints for SimpleDB can be retrieved from here:
 *
 * See also http://docs.amazonwebservices.com/general/latest/gr/rande.html#sdb_region
 * 
 * <table summary="List of AWS Regions and Endpoints">
 * 		<tr>
 *			<td><strong>Region</strong></td><td><strong>Endpoint</strong></td><td><strong>Location Constraint</strong></td><td><strong>Protocol</strong></td>
 *		</tr>
 *		<tr>
 *			<td>US Standard *</td><td>sdb.amazonaws.com</td><td>(none required)</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>US West (Oregon)</td><td>sdb-us-west-2.amazonaws.com</td><td>us-west-2</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>US West (Northern California)</td><td>sdb-us-west-1.amazonaws.com</td><td>us-west-1</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>EU (Ireland)</td><td>sdb-eu-west-1.amazonaws.com</td><td>EU</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>Asia Pacific (Singapore)</td><td>sdb-ap-southeast-1.amazonaws.com</td><td>ap-southeast-1</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>Asia Pacific (Tokyo)</td><td>sdb-ap-northeast-1.amazonaws.com</td><td>ap-northeast-1</td><td>HTTP and HTTPS</td>
 *		</tr>
 *		<tr>
 *			<td>South America (Sao Paulo)</td><td>sdb-sa-east-1.amazonaws.com</td><td>sa-east-1</td><td>HTTP and HTTPS</td>
 *		</tr>
 *	</table>
 */
//@formatter:on
abstract class AbstractSimpleDbClient {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String DEFAULT_REGION = "sdb.eu-west-1.amazonaws.com";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private AmazonSimpleDB _amazonSimpleDb = null;
	private String _amazonSimpleDbDomainName = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String ERROR_CODE_SERVICE_UNAVAILABLE = "ServiceUnavailable";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the SimpleDB support by directly providing all needed
	 * information to setup the instance.
	 * 
	 * @param aDomainName The name of the SimpleDB domain to use.
	 * @param aAccessKey The Amazon access key to use.
	 * @param aSecretKey The Amazon secret key to use.
	 */
	public AbstractSimpleDbClient( String aDomainName, String aAccessKey, String aSecretKey ) {
		this( aDomainName, aAccessKey, aSecretKey, DEFAULT_REGION );
	}

	//@formatter:off
	/**
	 * Constructs the SimpleDB support by directly providing all needed
	 * information to setup the instance. An end-point can be specified, i.e. the
	 * region where Amazon will store the data. 
	 * 
	 * Possible endpoints for SimpleDB can be retrieved as stated above.
	 * 
	 * {@link http://docs.amazonwebservices.com/general/latest/gr/rande.html#sdb_region}
	 * 
	 * @param aDomainName The name of the SimpleDB domain to use.
	 * 
	 * @param aAccessKey The Amazon access key to use.
	 * 
	 * @param aSecretKey The Amazon secret key to use.
	 * 
	 * @param aRegion The end-point (Amazon region) to use.
	 */
	//@formatter:on
	public AbstractSimpleDbClient( String aDomainName, String aAccessKey, String aSecretKey, String aRegion ) {
		if ( aRegion == null ) {
			aRegion = DEFAULT_REGION;
		}
		_amazonSimpleDb = AmazonSimpleDBClientBuilder.standard().withCredentials( new AWSStaticCredentialsProvider( new BasicAWSCredentials( aAccessKey, aSecretKey ) ) ).withRegion( aRegion ).build();
		_amazonSimpleDbDomainName = aDomainName;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Retrieves the domain name to be used.
	 * 
	 * @return The domain name.
	 */
	protected String getAmazonSimpleDbDomainName() {
		return _amazonSimpleDbDomainName;
	}

	/**
	 * Sets the domain name to be used.
	 *
	 * @param aAmazonSimpleDbDomainName the new amazon simple db domain name
	 */
	protected void setAmazonSimpleDbDomainName( String aAmazonSimpleDbDomainName ) {
		_amazonSimpleDbDomainName = aAmazonSimpleDbDomainName;
	}

	/**
	 * Retrieves the amazon SimpleDB client to be used.
	 * 
	 * @return The SimpleDB client to be used.
	 */
	protected AmazonSimpleDB getAmazonSimpleDbClient() {
		return _amazonSimpleDb;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Checks if is request timeout exception.
	 *
	 * @param aException the exception
	 * 
	 * @return true, if is request timeout exception
	 */
	protected static boolean isRequestTimeoutException( Exception aException ) {
		return ( aException instanceof RequestTimeoutException );
	}

	/**
	 * Checks if is service unavailable exception.
	 *
	 * @param aException the exception
	 * 
	 * @return true, if is service unavailable exception
	 */
	protected static boolean isServiceUnavailableException( Exception aException ) {
		if ( !( aException instanceof AmazonServiceException theAmazonServiceException ) ) {
			return false;
		}
		return ( ERROR_CODE_SERVICE_UNAVAILABLE.equalsIgnoreCase( theAmazonServiceException.getErrorCode() ) );
	}

	/**
	 * Creates an Amazon AWS specific exception message from the given throwable
	 * containing additional information such as the AWS error code, the AWS
	 * error type, the request ID, the service name and the status code.
	 * 
	 * @param aThrowable The throwable from which to generate the Amazon AWS
	 *        specific exception message.
	 * 
	 * @return The according exception message.
	 */
	protected static String toMessage( Throwable aThrowable ) {
		String theMessage = Trap.asMessage( aThrowable );
		if ( aThrowable instanceof AmazonServiceException ) {
			if ( theMessage != null && theMessage.length() > 0 && !theMessage.endsWith( "." ) && !theMessage.endsWith( "!" ) && !theMessage.endsWith( "?" ) && !theMessage.endsWith( ":" ) ) {
				theMessage += ".";
			}
			final AmazonServiceException awsException = (AmazonServiceException) aThrowable;
			theMessage += " AWS error code = \"" + awsException.getErrorCode() + "\" , AWS error type = <" + awsException.getErrorType() + ">, request ID = \"" + awsException.getRequestId() + "\", service name = \"" + awsException.getServiceName() + "\", status code = <" + awsException.getStatusCode() + ">.";
		}
		return theMessage;
	}

	/**
	 * Clears all content from the given Amazon SimpleDB domain.
	 * 
	 * @param aAmazonSimpleDbClient The {@link AmazonSimpleDBClient}.
	 * @param aDomainName The domain name of the domain to be cleared.
	 */
	protected static void clearDomain( AmazonSimpleDB aAmazonSimpleDbClient, String aDomainName ) {
		aAmazonSimpleDbClient.deleteDomain( new DeleteDomainRequest( aDomainName ) );
		aAmazonSimpleDbClient.createDomain( new CreateDomainRequest( aDomainName ) );
	}

	/**
	 * Retrieves an {@link AmazonSimpleDBClient} from a configuration file
	 * containing the access- and the secret key.
	 * 
	 * @param aConfigFile The configuration file used to configure the
	 *        {@link AmazonSimpleDBClient}.
	 * 
	 * @return An {@link AmazonSimpleDBClient}.
	 * 
	 * @throws IOException In case there were problems reading the configuration
	 *         file.
	 */
	protected static AmazonSimpleDB getAmazonSimpleDbClient( File aConfigFile ) throws IOException {
		final Properties theProperties = new Properties();
		theProperties.load( new FileInputStream( aConfigFile ) );
		return AmazonSimpleDBClientBuilder.standard().withCredentials( new AWSStaticCredentialsProvider( new PropertiesCredentials( aConfigFile ) ) ).build();
	}

	/**
	 * Retrieves a list of domain names retrievable from the given
	 * {@link AmazonSimpleDBClient}.
	 * 
	 * @param aAmazonSimpleDbClient The {@link AmazonSimpleDBClient}.
	 * 
	 * @return A list containing {@link String} instances representing the
	 *         retrievable domain names.
	 */
	protected static List<String> getDomainNames( AmazonSimpleDBClient aAmazonSimpleDbClient ) {
		final List<String> theDomainNames = new ArrayList<>();
		final ListDomainsResult theListDomainsResult = aAmazonSimpleDbClient.listDomains();
		for ( String eDomainName : theListDomainsResult.getDomainNames() ) {
			theDomainNames.add( eDomainName );
		}
		return theDomainNames;
	}

	/**
	 * Tests whether the given domain exists in Amazon SimpleDB.
	 * 
	 * @param aAmazonSimpleDbClient The {@link AmazonSimpleDBClient}.
	 * @param aDomainName The domain name to be tested.
	 * 
	 * @return True in case the domain with the given name exists, else false.
	 */
	protected static boolean hasDomain( AmazonSimpleDBClient aAmazonSimpleDbClient, String aDomainName ) {
		final DomainMetadataRequest theDomainMetadataRequest = new DomainMetadataRequest();
		theDomainMetadataRequest.setDomainName( aDomainName );
		try {
			aAmazonSimpleDbClient.domainMetadata( theDomainMetadataRequest );
		}
		catch ( NoSuchDomainException e ) {
			return false;
		}
		return true;
	}
}
