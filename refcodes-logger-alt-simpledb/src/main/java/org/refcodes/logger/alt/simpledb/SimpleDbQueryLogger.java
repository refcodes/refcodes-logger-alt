// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.simpledb;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Level;

import org.refcodes.criteria.Criteria;
import org.refcodes.criteria.ExpressionQueryFactory;
import org.refcodes.exception.Trap;
import org.refcodes.logger.Logger;
import org.refcodes.logger.QueryLogger;
import org.refcodes.tabular.Column;
import org.refcodes.tabular.ColumnFactory;
import org.refcodes.tabular.ColumnMismatchException;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.HeaderMismatchException;
import org.refcodes.tabular.RecordImpl;
import org.refcodes.tabular.Records;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.DeleteAttributesRequest;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;

/**
 * The {@link SimpleDbQueryLogger} extends the {@link SimpleDbLogger}
 * implementation with the {@link QueryLogger} functionality.
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public class SimpleDbQueryLogger<T> extends SimpleDbLogger<T> implements QueryLogger<T> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger( SimpleDbQueryLogger.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String SQL_WILDCARD = "*";
	private static final String SQL_WHERE = "WHERE";
	private static final String SQL_FROM = "FROM";
	private static final String SQL_SELECT = "SELECT";
	private static final String SQL_LIMIT = "LIMIT";
	private static final int MAX_LIMIT = 250;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ExpressionQueryFactory _expressionQueryFactory = new ExpressionQueryFactory();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link SimpleDbQueryLogger} for a given SimpleDB domain.
	 *
	 * @param aDomainName The name for the Amazon SimpleDB domain
	 * @param aAccessKey The Amazon access key for Amazon SimpleDB
	 * @param aSecretKey The Amazon secret key for Amazon SimpleDB
	 * @param aColumnFactory The {@link ColumnFactory} to create default
	 *        {@link Column} instances for {@link Record} instances to be
	 *        processed
	 */
	public SimpleDbQueryLogger( String aDomainName, String aAccessKey, String aSecretKey, ColumnFactory<T> aColumnFactory ) {
		super( aDomainName, aAccessKey, aSecretKey, null, aColumnFactory );
	}

	/**
	 * Constructs the {@link SimpleDbQueryLogger} for a given SimpleDB domain.
	 * 
	 * @param aDomainName The name for the Amazon SimpleDB domain
	 * @param aAccessKey The Amazon access key for Amazon SimpleDB
	 * @param aSecretKey The Amazon secret key for Amazon SimpleDB
	 * @param aEndPoint The end-point (Amazon region) to use (see
	 *        {@link AbstractSimpleDbClient}'s constructor documentation for
	 *        possible values).
	 * @param aColumnFactory The {@link ColumnFactory} to create default
	 *        {@link Column} instances for {@link Record} instances to be
	 *        processed
	 */
	public SimpleDbQueryLogger( String aDomainName, String aAccessKey, String aSecretKey, String aEndPoint, ColumnFactory<T> aColumnFactory ) {
		super( aDomainName, aAccessKey, aSecretKey, aEndPoint, aColumnFactory );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs() {
		return findLogs( null, null, -1 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( int aLimit ) {
		return findLogs( null, null, aLimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( Criteria aCriteria ) {
		return findLogs( aCriteria, null, -1 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( Criteria aCriteria, int aLimit ) {
		return findLogs( aCriteria, null, aLimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( Criteria aCriteria, Header<T> aHeader ) {
		return findLogs( aCriteria, aHeader, -1 );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( Header<T> aHeader, int aLimit ) {
		return findLogs( null, aHeader, aLimit );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Records<T> findLogs( Criteria aCriteria, Header<T> aHeader, int aLimit ) {
		flushBuffer();
		final String theSelectQuery = toSqlQuery( aCriteria, aLimit );
		return new SimpleDbRecords( theSelectQuery, aHeader );
	}

	// /////////////////////////////////////////////////////////////////////////
	// COMPONENT:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void destroy() {
		super.destroy();
		_expressionQueryFactory = null;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates an SQL SELECT query from the given {@link Criteria} and the
	 * provided limit.
	 * 
	 * @param aCriteria The Criteria from which to create the SQL query.
	 * @param aLimit The limit to be applied to the result set of the query.
	 * 
	 * @return A (No)SQL query for SimpleDB.
	 */
	protected String toSqlQuery( Criteria aCriteria, int aLimit ) {
		if ( aLimit > MAX_LIMIT ) {
			LOGGER.log( Level.WARNING, "This implementation only supports a max limit of <" + MAX_LIMIT + ">, you provided a limit of <" + aLimit + ">, your result will be limited to the max limit." );
		}
		String theSelectQuery = SQL_SELECT + " " + SQL_WILDCARD + " " + SQL_FROM + getAmazonSimpleDbDomainName() + " " + SQL_WHERE + " " + _expressionQueryFactory.fromCriteria( aCriteria );
		if ( aLimit != -1 ) {
			theSelectQuery += " " + SQL_LIMIT + " " + aLimit;
		}
		LOGGER.info( "Created query \"" + theSelectQuery + "\"..." );
		return theSelectQuery;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * This iterator encapsulates an Amazon select request for the business
	 * logic to simply iterate through the results.
	 */
	protected class SimpleDbRecords implements Records<T> {

		private SelectRequest _selectRequest;
		private Header<T> _header;
		private Iterator<Item> _itemPage = null;
		private String _nextToken = "";
		private Item _nextItem = null;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Constructs a {@link Records} iterator from a SimpleDB query and the
		 * required {@link Header}.
		 * 
		 * @param aSelectQuery The query for retrieval.
		 */
		public SimpleDbRecords( String aSelectQuery ) {
			this( aSelectQuery, null );
		}

		/**
		 * Constructs a {@link Records} iterator from a SimpleDB query and the
		 * required {@link Header}.
		 * 
		 * @param aSelectQuery The query for retrieval.
		 * @param aHeader The expected {@link Header} in the result.
		 */
		public SimpleDbRecords( String aSelectQuery, Header<T> aHeader ) {
			_selectRequest = new SelectRequest( aSelectQuery );
			_header = aHeader;
			doPrepareNext();
		}

		// /////////////////////////////////////////////////////////////////////
		// ITERATOR:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Prepares retrieving the next batch of rows. Thereby runtime
		 * exceptions may get thrown. In the constructor, those exceptions are
		 * passed to the caller!
		 */
		private void doPrepareNext() {
			_selectRequest.setNextToken( _nextToken );
			final SelectResult theSelectResult = getAmazonSimpleDbClient().select( _selectRequest );
			_nextToken = theSelectResult.getNextToken();
			_itemPage = theSelectResult.getItems().iterator();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean hasNext() {
			if ( _itemPage == null || !_itemPage.hasNext() ) {
				if ( _nextToken != null ) {
					try {
						doPrepareNext();
						return _itemPage.hasNext();
					}
					catch ( Exception e ) {
						LOGGER.log( Level.WARNING, "Caught an exception (Amazon domain is \"" + getAmazonSimpleDbDomainName() + "\") of type \"" + e.getClass().getName() + "\" on Amazon domain \"" + getAmazonSimpleDbDomainName() + "\" as of: " + Trap.asMessage( e ), e );
						return false;
					}
				}
			}
			return _itemPage.hasNext();
		}

		/**
		 * "Raw" method to retrieve the next item from Simple DB. Needed for the
		 * {@link SimpleDbTrimLogger#deleteLogs(Criteria)} operation.
		 *
		 * @return the item
		 */
		protected synchronized Item nextItem() {

			if ( hasNext() ) {
				_nextItem = _itemPage.next();
				return _nextItem;
			}
			else {
				throw new NoSuchElementException( "No more elements can be retrieved from the given SimpleDB select queiry \"" + _selectRequest.getSelectExpression() + "\"!" );
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public synchronized org.refcodes.tabular.Record<T> next() {
			if ( hasNext() ) {
				org.refcodes.tabular.Record<String[]> theFromStorageRecord = new RecordImpl<>();
				org.refcodes.tabular.Record<T> theRecord = null;
				List<String> eList;
				_nextItem = _itemPage.next();
				for ( Attribute eAttribute : _nextItem.getAttributes() ) {
					addHeaderColumn( eAttribute.getName() );
					if ( theFromStorageRecord.containsKey( eAttribute.getName() ) ) {
						final Object eValue = theFromStorageRecord.get( eAttribute.getName() );
						// Array attribute value already in record:
						if ( eValue instanceof String[] ) {
							eList = new ArrayList<>( Arrays.asList( (String[]) eValue ) );
							eList.add( eAttribute.getValue() );
							theFromStorageRecord.put( eAttribute.getName(), eList.toArray( new String[eList.size()] ) );
						}
						// Plain attribute value already in record:
						else {
							theFromStorageRecord.put( eAttribute.getName(), new String[] { (String) eValue, eAttribute.getValue() } );
						}
					}
					// Attribute value not yet in record:
					else {
						if ( getHeader().containsKey( eAttribute.getName() ) ) {
							// Column say array type:
							final Column<?> theColumn = getHeader().get( eAttribute.getName() );
							if ( theColumn.getType().isArray() ) {
								theFromStorageRecord.put( eAttribute.getName(), new String[] { eAttribute.getValue() } );
							}
							// Columns says plain type:
							else {
								theFromStorageRecord.put( eAttribute.getName(), new String[] { eAttribute.getValue() } );
							}
						}
						// No header column known:
						else {
							theFromStorageRecord.put( eAttribute.getName(), new String[] { eAttribute.getValue() } );
						}
					}
				}
				try {
					// Columns provided:
					if ( _header != null ) {
						theRecord = _header.fromStorageStrings( theFromStorageRecord );
						// Reduce result columns to the ones desired:
						theFromStorageRecord = _header.toSubset( theFromStorageRecord );

					}
					// No columns provided, using standard header:
					else {
						theRecord = getHeader().fromStorageStrings( theFromStorageRecord );
					}
				}
				catch ( ParseException | HeaderMismatchException | ColumnMismatchException e ) {
					LOGGER.log( Level.WARNING, "The retrieved record on Amazon domain \"" + getAmazonSimpleDbDomainName() + "\" with keys {" + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theFromStorageRecord.keySet() ).withDelimiter( ',' ).toRecord() + "} and values {" + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( theFromStorageRecord.values() ).withDelimiter( ',' ).toRecord() + "} cannot be converted to the provided header {" + new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withFields( _header.keySet() ).withDelimiter( ',' ).toRecord() + "} format as of: " + Trap.asMessage( e ), e );
				}
				return ( theRecord == null ? new RecordImpl<T>() : theRecord );
			}
			else {
				throw new NoSuchElementException( "No more elements can be retrieved from the given SimpleDB select query \"" + _selectRequest.getSelectExpression() + "\"!" );
			}
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public synchronized void remove() {
			if ( _nextItem != null ) {
				try {
					getAmazonSimpleDbClient().deleteAttributes( new DeleteAttributesRequest( getAmazonSimpleDbDomainName(), _nextItem.getName() ) );
				}
				catch ( AmazonServiceException e ) {
					throw new NoSuchElementException( "No element can be deleted from the given SimpleDB being queried with the select query \"" + _selectRequest.getSelectExpression() + "\": " + Trap.asMessage( e ) );
				}
				finally {
					_nextItem = null;
				}
			}
			else {
				throw new NoSuchElementException( "No element can be deleted from the given SimpleDB being queried with the select query \"" + _selectRequest.getSelectExpression() + "\"!" );
			}
		}
	}
}
