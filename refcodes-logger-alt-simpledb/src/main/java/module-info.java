module org.refcodes.logger.alt.simpledb {
	requires org.refcodes.exception;
	requires transitive org.refcodes.criteria;
	requires org.refcodes.logger;
	requires org.refcodes.tabular;
	requires aws.java.sdk.core;
	requires aws.java.sdk.simpledb;
	// requires software.amazon.awssdk.core;
	// requires software.amazon.awssdk.profiles;
	// requires software.amazon.awssdk.auth;

	exports org.refcodes.logger.alt.simpledb;
}
