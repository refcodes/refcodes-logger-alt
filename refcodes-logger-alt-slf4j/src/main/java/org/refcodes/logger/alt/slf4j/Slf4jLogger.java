// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.logger.alt.slf4j;

import org.refcodes.logger.IllegalRecordRuntimeException;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.Logger;
import org.refcodes.logger.LoggerField;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerImpl;
import org.refcodes.logger.RuntimeLoggerSingleton;
import org.refcodes.mixin.NameAccessor.NameMutator;
import org.refcodes.runtime.Execution;
import org.refcodes.tabular.ColumnMismatchException;
import org.refcodes.tabular.Record;
import org.slf4j.LoggerFactory;

/**
 * The {@link Slf4jLogger} delegates all logs to an according
 * {@link org.slf4j.Logger} instance. This logger may be used as underlying
 * {@link Logger} for the {@link RuntimeLogger} ({@link RuntimeLoggerImpl}).
 *
 * @author steiner
 */
public class Slf4jLogger implements Logger<Object>, NameMutator {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private transient org.slf4j.Logger _logger;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link Slf4jLogger} by determining the underlying
	 * {@link org.slf4j.Logger}'s name from the caller's stack trace. See also
	 * {@link #setName(String)}.
	 */
	public Slf4jLogger() {
		StackTraceElement theCaller = Execution.probeTillNoneLoggerElement( RuntimeLoggerImpl.class, RuntimeLogger.class, RuntimeLoggerSingleton.class, Slf4jLogger.class );
		_logger = LoggerFactory.getLogger( theCaller.getClassName() );
	}

	/**
	 * Instantiates a new {@link Slf4jLogger} by setting the underlying
	 * {@link org.slf4j.Logger}'s name with the provided name. See also
	 * {@link #setName(String)}.
	 *
	 * @param aName The name to be used for the logger.
	 */
	public Slf4jLogger( String aName ) {
		_logger = LoggerFactory.getLogger( aName );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Record<? extends Object> aRecord ) {
		try {
			final LogPriority thePriority = (LogPriority) LoggerField.LOG_PRIORITY.getColumn().get( aRecord );
			// Integer theLineNumber = (Integer) LoggerField.LOG_LINE_NUMBER.getColumn().get( aRecord );
			// String theMessage = (String) LoggerField.LOG_MESSAGE.getColumn().get( aRecord );
			//	if ( Correlation.hasAnyCorrelationId() ) {
			//		theMessage = Correlation.toFullCorrelationId() + " " + theMessage;
			//	}
			// String theMethodName = (String) LoggerField.LOG_METHODE_NAME.getColumn().get( aRecord );
			// String theLogMessage = theMessage + " " + theMethodName + "@line:" + theLineNumber;
			// final String theClassName = (String) LoggerField.LOG_FULLY_QUALIFIED_CLASS_NAME.getColumn().get( aRecord );
			final String theLogMessage = (String) LoggerField.LOG_MESSAGE.getColumn().get( aRecord );
			final Exception theException = (Exception) LoggerField.LOG_NONE_STACKTRRACE_EXCEPTION.getColumn().get( aRecord );

			// @formatter:off
			switch ( thePriority ) {
				case PANIC -> _logger.error( theLogMessage, theException );
				case ALERT -> _logger.error( theLogMessage, theException );
				case CRITICAL -> _logger.error( theLogMessage, theException );
				case ERROR -> _logger.error( theLogMessage, theException );
				case WARN -> _logger.warn( theLogMessage, theException );
				case NOTICE -> _logger.info( theLogMessage, theException );
				case INFO -> _logger.info( theLogMessage, theException );
				case DEBUG -> _logger.debug( theLogMessage, theException );
				case TRACE -> _logger.trace( theLogMessage, theException );
				case NONE -> {}
				case DISCARD -> {}
			}
			// @formatter:on
		}
		catch ( ColumnMismatchException | ClassCastException e ) {
			throw new IllegalRecordRuntimeException( aRecord, e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setName( String aName ) {
		_logger = LoggerFactory.getLogger( aName );
	}
}
