module org.refcodes.logger.alt.slf4j {
	requires org.refcodes.runtime;
	requires transitive org.refcodes.logger;
	requires transitive org.refcodes.tabular;
	requires transitive org.slf4j;

	exports org.refcodes.logger.alt.slf4j;
}
