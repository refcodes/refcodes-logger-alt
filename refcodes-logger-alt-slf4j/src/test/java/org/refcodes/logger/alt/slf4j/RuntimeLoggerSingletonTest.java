package org.refcodes.logger.alt.slf4j;

import org.junit.jupiter.api.Test;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerSingleton;

// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
public class RuntimeLoggerSingletonTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testRuntimeLoggerSingleton() {
		final RuntimeLogger theRuntimeLogger = RuntimeLoggerSingleton.getInstance();
		doTestLogger( theRuntimeLogger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void doTestLogger( RuntimeLogger aRuntimeLogger ) {
		aRuntimeLogger.notice( "Hello {0} {1}, doing logger test {2}-{3}-{4}:", "Nolan", "Bushnell", 3, 2, 1 );
		aRuntimeLogger.trace( "Hallo Straße!" );
		aRuntimeLogger.debug( "Hallo Stadt!" );
		aRuntimeLogger.info( "Hallo Mond!" );
		// aRuntimeLogger.warn( "Hallo Mars!", new RuntimeException( "Mars exception!" ) );
		// aRuntimeLogger.error( "Hallo Saturn!", new RuntimeException( "Saturn exception!" ) );
		// aRuntimeLogger.panic( "Hallo Milchstraße!", new RuntimeException( "Milchstraße exception!" ) );
		System.out.flush();
		System.err.flush();
	}
}
