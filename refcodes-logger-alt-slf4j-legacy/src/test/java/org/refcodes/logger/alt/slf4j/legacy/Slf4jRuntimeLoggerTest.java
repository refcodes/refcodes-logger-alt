// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.slf4j.legacy;

import org.junit.jupiter.api.Test;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerImpl;

public class Slf4jRuntimeLoggerTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSlf4jRuntimeLoggerImpl() {
		final Slf4jLogger theLogger = new Slf4jLogger();
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( theLogger, LogPriority.TRACE );
		doTestLogger( theRuntimeLogger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Tests with a given runtime logger.
	 * 
	 * @param aRuntimeLogger The runtime logger with which to perform the tests.
	 */
	private void doTestLogger( RuntimeLogger aRuntimeLogger ) {
		aRuntimeLogger.trace( "Hallo Straße!" );
		aRuntimeLogger.debug( "Hallo Stadt!" );
		aRuntimeLogger.info( "Hallo Mond!" );
		// aRuntimeLogger.warn( "Hallo Mars!", new RuntimeException( "Mars exception!" ) );
		// aRuntimeLogger.error( "Hallo Saturn!", new RuntimeException( "Saturn exception!" ) );
		// aRuntimeLogger.panic( "Hallo Milchstraße!", new RuntimeException( "Milchstraße exception!" ) );
		System.out.flush();
		System.err.flush();
	}
}
