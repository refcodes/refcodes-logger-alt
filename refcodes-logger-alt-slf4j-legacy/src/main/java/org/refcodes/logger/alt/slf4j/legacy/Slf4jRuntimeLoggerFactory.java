// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.logger.alt.slf4j.legacy;

import java.util.Map;

import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactory;
import org.refcodes.runtime.Execution;
import org.slf4j.LoggerFactory;

/**
 * The Class Slf4jRuntimeLoggerFactory.
 */
public class Slf4jRuntimeLoggerFactory implements RuntimeLoggerFactory {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new slf 4 j runtime logger factory impl.
	 */
	protected Slf4jRuntimeLoggerFactory() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RuntimeLogger create() {
		// ---------------------------------------------------------------------
		// ATTENTION: As the StackTraceElement caller array is evaluated, we
		// must not delegate this call to one "master" implementation as this
		// will falsify the actual origin of this factory call.
		// ---------------------------------------------------------------------
		final String className = Execution.getCallerStackTraceElement( Slf4jRuntimeLoggerFactory.class ).getClassName();
		return new Slf4jRuntimeLogger( LoggerFactory.getLogger( className ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	// ---------------------------------------------------------------------
	// ATTENTION: As the StackTraceElement caller array is evaluated, we
	// must not delegate this call to one "master" implementation as this
	// will falsify the actual origin of this factory call.
	// ---------------------------------------------------------------------
	public RuntimeLogger create( Map<String, String> aProperties ) {
		return new Slf4jRuntimeLogger( LoggerFactory.getLogger( Execution.getCallerStackTraceElement( Slf4jRuntimeLoggerFactory.class ).getClassName() ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	// ---------------------------------------------------------------------
	// ATTENTION: As the StackTraceElement caller array is evaluated, we
	// must not delegate this call to one "master" implementation as this
	// will falsify the actual origin of this factory call.
	// ---------------------------------------------------------------------
	public RuntimeLogger create( String aIdentifier ) {
		return new Slf4jRuntimeLogger( LoggerFactory.getLogger( aIdentifier ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	// -------------------------------------------------------------------------
	// ATTENTION: As the StackTraceElement caller array is evaluated, we
	// must not delegate this call to one "master" implementation as this
	// will falsify the actual origin of this factory call.
	// -------------------------------------------------------------------------
	public RuntimeLogger create( String aIdentifier, Map<String, String> aProperties ) {
		return new Slf4jRuntimeLogger( LoggerFactory.getLogger( aIdentifier ) );
	}
}
