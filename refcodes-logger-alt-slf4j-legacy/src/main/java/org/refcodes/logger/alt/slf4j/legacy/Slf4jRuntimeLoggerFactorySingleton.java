// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.logger.alt.slf4j.legacy;

import java.util.Map;

import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactory;

/**
 * The Class Slf4jRuntimeLoggerFactorySingleton.
 *
 * @author steiner
 */
public class Slf4jRuntimeLoggerFactorySingleton extends Slf4jRuntimeLoggerFactory {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static final Slf4jRuntimeLoggerFactorySingleton _runtimeLoggerFactorySingleton = new Slf4jRuntimeLoggerFactorySingleton();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new slf 4 j runtime logger factory singleton.
	 */
	protected Slf4jRuntimeLoggerFactorySingleton() {}

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link Slf4jRuntimeLoggerFactorySingleton}.
	 * 
	 * @return The {@link RuntimeLoggerFactory} singleton's instance.
	 */
	public static RuntimeLoggerFactory getInstance() {
		return _runtimeLoggerFactorySingleton;
	}

	/**
	 * Convenience method actually delegating the call to the instance returned
	 * by {@link #getInstance()}. See also {@link RuntimeLoggerFactory#create()}
	 *
	 * @return the runtime logger
	 */
	public static RuntimeLogger createRuntimeLogger() {
		return getInstance().create();
	}

	/**
	 * Convenience method actually delegating the call to the instance returned
	 * by {@link #getInstance()}. See also
	 * {@link RuntimeLoggerFactory#create(Map)}
	 *
	 * @param aProperties the properties
	 * 
	 * @return the runtime logger
	 */
	public static RuntimeLogger createRuntimeLogger( Map<String, String> aProperties ) {
		return getInstance().create( aProperties );
	}

	/**
	 * Convenience method actually delegating the call to the instance returned
	 * by {@link #getInstance()}. See also
	 * {@link RuntimeLoggerFactory#create(Map)}
	 *
	 * @param aIdentifier the identifier
	 * 
	 * @return the runtime logger
	 */
	public static RuntimeLogger createRuntimeLogger( String aIdentifier ) {
		return getInstance().create( aIdentifier );
	}

	/**
	 * Convenience method actually delegating the call to the instance returned
	 * by {@link #getInstance()}. See also
	 * {@link RuntimeLoggerFactory#create(Object, Map)}
	 *
	 * @param aIdentifier the identifier
	 * @param aProperties the properties
	 * 
	 * @return the runtime logger
	 */
	public RuntimeLogger createRuntimeLogger( String aIdentifier, Map<String, String> aProperties ) {
		return getInstance().create( aIdentifier, aProperties );
	}
}
