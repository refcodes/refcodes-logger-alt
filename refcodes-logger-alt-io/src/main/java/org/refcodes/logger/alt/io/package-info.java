// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact extends the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-logger">refcodes-logger</a>
 * toolkit with I/O logging functionality (supporting
 * {@link java.io.PrintStream}, {@link java.io.OutputStream} and
 * {@link java.io.File} types as datasinks) as of the
 * {@link org.refcodes.logger.alt.io.IoLogger} type.
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-logger"><strong>refcodes-logger:
 * Fancy runtime-logs and highly scalable cloud logging</strong></a>
 * documentation for an up-to-date and detailed description on the usage of this
 * artifact.
 * </p>
 */
package org.refcodes.logger.alt.io;
