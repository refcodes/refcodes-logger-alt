// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.refcodes.data.Delimiter;
import org.refcodes.logger.IllegalRecordRuntimeException;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.Logger;
import org.refcodes.logger.LoggerField;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.logger.RuntimeLoggerHeader;
import org.refcodes.mixin.DelimiterAccessor.DelimiterBuilder;
import org.refcodes.mixin.DelimiterAccessor.DelimiterProperty;
import org.refcodes.mixin.ErrorPrintStreamAccessor.ErrorPrintStreamMutator;
import org.refcodes.mixin.PrintStreamAccessor.PrintStreamMutator;
import org.refcodes.mixin.TrimAccessor.TrimBuilder;
import org.refcodes.mixin.TrimAccessor.TrimProperty;
import org.refcodes.tabular.ColumnMismatchException;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.HeaderMismatchException;
import org.refcodes.tabular.PrintStackTrace;
import org.refcodes.tabular.Record;
import org.refcodes.tabular.Row;
import org.refcodes.textual.CsvBuilder;
import org.refcodes.textual.CsvEscapeMode;
import org.refcodes.textual.CsvEscapeModeAccessor.CsvEscapeModeBuilder;
import org.refcodes.textual.CsvEscapeModeAccessor.CsvEscapeModeProperty;

/**
 * The {@link IoLogger} implements the {@link Logger} interface for providing
 * logging functionality for I/O output (e.g. via {@link System#out} and
 * {@link System#err} by default).
 *
 * @param <T> The type of the {@link Record} instances managed by the
 *        {@link Logger}.
 */
public class IoLogger<T> implements Logger<T>, PrintStreamMutator, ErrorPrintStreamMutator, CsvEscapeModeProperty, CsvEscapeModeBuilder<IoLogger<T>>, TrimProperty, TrimBuilder<IoLogger<T>>, DelimiterProperty, DelimiterBuilder<IoLogger<T>> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	static {
		RuntimeLoggerFactorySingleton.bindJavaUtilLogging();
	}

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String PROPERTY_ERROR_STREAM = "ERROR_STREAM";
	public static final String PROPERTY_OUTPUT_STREAM = "OUTPUT_STREAM";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Header<T> _header = null;
	protected PrintStream _errorStream = System.err;
	protected PrintStream _outputStream = System.out;
	protected CsvBuilder _csvBuilder;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aFile The {@link File} to be used for printing.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public IoLogger( Header<T> aHeader, File aFile ) throws FileNotFoundException {
		this( aHeader, new PrintStream( aFile ), new PrintStream( aFile ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aFile The {@link File} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public IoLogger( Header<T> aHeader, File aFile, char aCsvDelimiter ) throws FileNotFoundException {
		this( aHeader, new PrintStream( aFile ), new PrintStream( aFile ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link IoLogger}.
	 * 
	 * @param aInputFile The {@link File} to be used for printing output.
	 * @param aErrorFile The {@link File} to be used for printing errors.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public IoLogger( File aInputFile, File aErrorFile ) throws FileNotFoundException {
		this( (Header<T>) null, new PrintStream( aInputFile ), new PrintStream( aErrorFile ) );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aInputFile The {@link File} to be used for printing output.
	 * @param aErrorFile The {@link File} to be used for printing errors.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public IoLogger( Header<T> aHeader, File aInputFile, File aErrorFile ) throws FileNotFoundException {
		this( aHeader, new PrintStream( aInputFile ), new PrintStream( aErrorFile ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link IoLogger}.
	 * 
	 * @param aFile The {@link File} to be used for writing to.
	 * 
	 * @throws FileNotFoundException If the given file object does not denote an
	 *         existing, writable regular file and a new regular file of that
	 *         name cannot be created, or if some other error occurs while
	 *         opening or creating the file.
	 */
	public IoLogger( File aFile ) throws FileNotFoundException {
		this( (Header<T>) null, new PrintStream( aFile ) );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} to be used for printing.
	 */
	public IoLogger( Header<T> aHeader, OutputStream aOutputStream ) {
		this( aHeader, new PrintStream( aOutputStream ), new PrintStream( aOutputStream ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public IoLogger( Header<T> aHeader, OutputStream aOutputStream, char aCsvDelimiter ) {
		this( aHeader, new PrintStream( aOutputStream ), new PrintStream( aOutputStream ), aCsvDelimiter );
	}

	/**
	 * Constructs a {@link IoLogger}.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be used for printing
	 *        output.
	 * @param aErrorStream The {@link OutputStream} to be used for printing
	 *        errors.
	 */
	public IoLogger( OutputStream aOutputStream, OutputStream aErrorStream ) {
		this( (Header<T>) null, new PrintStream( aOutputStream ), new PrintStream( aErrorStream ) );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aOutputStream The {@link OutputStream} to be used for printing
	 *        output.
	 * @param aErrorStream The {@link OutputStream} to be used for printing
	 *        errors.
	 */
	public IoLogger( Header<T> aHeader, OutputStream aOutputStream, OutputStream aErrorStream ) {
		this( aHeader, new PrintStream( aOutputStream ), new PrintStream( aErrorStream ), Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link IoLogger}.
	 * 
	 * @param aOutputStream The {@link OutputStream} to be used for printing.
	 */
	public IoLogger( OutputStream aOutputStream ) {
		this( (Header<T>) null, new PrintStream( aOutputStream ) );
	}

	/**
	 * Constructs a {@link IoLogger}.
	 * 
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 */
	public IoLogger( PrintStream aPrintStream ) {
		this( (Header<T>) null, aPrintStream, aPrintStream );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 */
	public IoLogger( Header<T> aHeader, PrintStream aPrintStream ) {
		this( aHeader, aPrintStream, aPrintStream, Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aPrintStream The {@link PrintStream} to be used for printing.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public IoLogger( Header<T> aHeader, PrintStream aPrintStream, char aCsvDelimiter ) {
		this( aHeader, aPrintStream, aPrintStream, aCsvDelimiter );
	}

	/**
	 * Constructs a {@link IoLogger}.
	 * 
	 * @param aOutputStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aErrorStream The {@link PrintStream} to be used for printing
	 *        errors.
	 */
	public IoLogger( PrintStream aOutputStream, PrintStream aErrorStream ) {
		this( (Header<T>) null, aOutputStream, aErrorStream );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aOutputStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aErrorStream The {@link PrintStream} to be used for printing
	 *        errors.
	 */
	public IoLogger( Header<T> aHeader, PrintStream aOutputStream, PrintStream aErrorStream ) {
		this( aHeader, aOutputStream, aErrorStream, Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aOutputStream The {@link PrintStream} to be used for printing
	 *        output.
	 * @param aErrorStream The {@link PrintStream} to be used for printing
	 *        errors.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public IoLogger( Header<T> aHeader, PrintStream aOutputStream, PrintStream aErrorStream, char aCsvDelimiter ) {
		this( aHeader, aCsvDelimiter );
		_errorStream = aOutputStream;
		_outputStream = aErrorStream;
	}

	/**
	 * Constructs a {@link IoLogger}.
	 */
	public IoLogger() {
		this( (Header<T>) null );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 */
	public IoLogger( Header<T> aHeader ) {
		this( aHeader, Delimiter.CSV.getChar() );
	}

	/**
	 * Constructs a {@link IoLogger} with the given header for logging.
	 * 
	 * @param aHeader The header used for logging in the correct format.
	 * @param aCsvDelimiter The separator to be used when separating the CSV
	 *        values in the log output.
	 */
	public IoLogger( Header<T> aHeader, char aCsvDelimiter ) {
		_csvBuilder = new CsvBuilder().withCsvEscapeMode( CsvEscapeMode.ESCAPED ).withDelimiter( aCsvDelimiter );
		_header = aHeader;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CsvEscapeMode getCsvEscapeMode() {
		return _csvBuilder.getCsvEscapeMode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTrim() {
		return _csvBuilder.isTrim();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _csvBuilder.getDelimiter();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setTrim( boolean isTrim ) {
		_csvBuilder.setTrim( isTrim );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDelimiter( char aDelimiter ) {
		_csvBuilder.setDelimiter( aDelimiter );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IoLogger<T> withTrim( boolean isTrimRecords ) {
		_csvBuilder.setTrim( isTrimRecords );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IoLogger<T> withCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		_csvBuilder.setCsvEscapeMode( aCsvEscapeMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IoLogger<T> withDelimiter( char aDelimiterChar ) {
		_csvBuilder.setDelimiter( aDelimiterChar );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCsvEscapeMode( CsvEscapeMode aCsvEscapeMode ) {
		_csvBuilder.setCsvEscapeMode( aCsvEscapeMode );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( Record<? extends T> aRecord ) {
		try {
			if ( getHeader() instanceof RuntimeLoggerHeader ) {
				final LogPriority thePriority = (LogPriority) LoggerField.LOG_PRIORITY.getColumn().get( aRecord );
				if ( thePriority != null ) {
					if ( thePriority != LogPriority.DISCARD ) {
						final Row<String> theRow = getHeader().toPrintableRow( aRecord );
						if ( thePriority.getPriority() >= LogPriority.WARN.getPriority() ) {
							_errorStream.println( _csvBuilder.toRecord( theRow ) );
						}
						else {
							_outputStream.println( _csvBuilder.toRecord( theRow ) );
						}
					}
				}
				else {
					final Row<String> theRow = getHeader().toPrintableRow( aRecord );
					_outputStream.println( _csvBuilder.toRecord( theRow ) );
				}

			}
			else {
				final Row<String> theRow = getHeader().toPrintableRow( aRecord );
				_outputStream.println( _csvBuilder.toRecord( theRow ) );
			}
		}
		catch ( ColumnMismatchException | ClassCastException | HeaderMismatchException e ) {
			throw new IllegalRecordRuntimeException( aRecord, e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setErrorPrintStream( PrintStream aErrorStream ) {
		_errorStream = aErrorStream;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPrintStream( PrintStream aOutputStream ) {
		_outputStream = aOutputStream;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the {@link Header} used by the {@link IoLogger} for usage by
	 * sub-classes.
	 * 
	 * @return The {@link Header} used by the {@link IoLogger}.
	 */
	@SuppressWarnings("unchecked")
	protected Header<T> getHeader() {
		if ( _header == null ) {
			synchronized ( this ) {
				_header = (Header<T>) new RuntimeLoggerHeader( PrintStackTrace.EXPLODED );
			}
		}
		return _header;
	}
}
