module org.refcodes.logger.alt.io {
	requires org.refcodes.data;
	requires org.refcodes.textual;
	requires transitive org.refcodes.logger;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.tabular;

	exports org.refcodes.logger.alt.io;
}
