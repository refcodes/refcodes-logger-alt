// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerHeader;
import org.refcodes.logger.RuntimeLoggerImpl;
import org.refcodes.tabular.Header;
import org.refcodes.tabular.PrintStackTrace;

/**
 * The Class IoRuntimeLoggerTest.
 */
public class IoRuntimeLoggerTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = LogManager.getLogger( IoRuntimeLoggerTest.class );

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testIoRuntimeLoggerImpl() {
		doReferenceLogger();
		final Header<Object> theHeader = new RuntimeLoggerHeader( PrintStackTrace.EXPLODED );
		final IoLogger<Object> theLogger = new IoLogger<>( theHeader );
		final RuntimeLogger theRuntimeLogger = new RuntimeLoggerImpl( theLogger, LogPriority.TRACE );
		doTestLogger( theRuntimeLogger );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private void doTestLogger( RuntimeLogger aRuntimeLogger ) {
		LOGGER.info( "------------------------------ Runtime logger log ------------------------------" );
		aRuntimeLogger.trace( "Hallo Straße!" );
		aRuntimeLogger.debug( "Hallo Stadt!" );
		aRuntimeLogger.info( "Hallo Mond!" );
		// aRuntimeLogger.warn( "Hallo Mars!", new RuntimeException( "Mars exception!" ) );
		// aRuntimeLogger.error( "Hallo Saturn!", new RuntimeException( "Saturn exception!" ) );
		// aRuntimeLogger.panic( "Hallo Milchstraße!", new RuntimeException( "Milchstraße exception!" ) );
		System.out.flush();
		System.err.flush();
	}

	private void doReferenceLogger() {
		LOGGER.info( "----------------------------- Reference Log4j log ------------------------------" );
		final Logger theLog4jLogger = LogManager.getLogger( getClass() );
		theLog4jLogger.trace( "Hallo Straße!" );
		theLog4jLogger.debug( "Hallo Stadt!" );
		theLog4jLogger.info( "Hallo Mond!" );
		// theLog4jLogger.warn( "Hallo Mars!", new RuntimeException( "Mars exception!" ) );
		// theLog4jLogger.error( "Hallo Saturn!", new RuntimeException( "Saturn exception!" ) );
		// theLog4jLogger.fatal( "Hallo Milchstraße!", new RuntimeException( "Milchstraße exception!" ) );
		System.out.flush();
		System.err.flush();
	}
}
