// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.logger.alt.spring;

import java.io.IOException;
import java.net.URI;

import org.refcodes.factory.BeanLookupFactory;
import org.refcodes.factory.alt.spring.SpringBeanFactory;
import org.refcodes.logger.LogPriority;
import org.refcodes.logger.Logger;
import org.refcodes.logger.LoggerInstantiationRuntimeException;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerImpl;
import org.refcodes.logger.RuntimeLoggerSingleton;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Less flexible shortcut for the class {@link RuntimeLoggerSingleton}. The
 * {@link SpringRuntimeLoggerSingleton} provides a {@link RuntimeLogger}
 * singleton from the class specified in the bean
 * {@link SpringRuntimeLoggerSingleton#RUNTIME_LOGGER_BEAN} declared by the
 * context file {@link SpringRuntimeLoggerSingleton#RUNTIME_LOGGER_CONTEXT}.
 */
public class SpringRuntimeLoggerSingleton extends RuntimeLoggerImpl {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The default Spring bean name for a refcodes {@link RuntimeLogger} bean.
	 */
	public static String RUNTIME_LOGGER_BEAN = "runtimeLogger";

	/**
	 * The filename of the file on the classpath containing the spring context
	 * for the runtime logger.
	 */
	public static final String RUNTIME_LOGGER_CONTEXT = "runtimelogger-context.xml";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static RuntimeLogger _runtimeLoggerSingleton;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new spring runtime logger singleton.
	 *
	 * @param aLogger the logger
	 * @param aPriority the priority
	 */
	protected SpringRuntimeLoggerSingleton( Logger<Object> aLogger, LogPriority aPriority ) {
		super( aLogger, aPriority );
	}

	// /////////////////////////////////////////////////////////////////////////
	// SINGLETON:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link SpringRuntimeLoggerSingleton}.
	 * 
	 * @return The {@link RuntimeLogger} singleton's instance.
	 * 
	 * @throws LoggerInstantiationRuntimeException Thrown in case instantiating
	 *         a {@link Logger} ({@link RuntimeLogger}) failed
	 */
	public static RuntimeLogger getInstance() {
		return getInstance( null );
	}

	/**
	 * Returns the singleton's instance as fabricated by this
	 * {@link SpringRuntimeLoggerSingleton}.
	 * 
	 * @param aLoggerBeanId The logger's bean TID as defined in the according
	 *        {@link #RUNTIME_LOGGER_CONTEXT} Spring context file.
	 * 
	 * @return The {@link RuntimeLogger} singleton's instance.
	 * 
	 * @throws LoggerInstantiationRuntimeException Thrown in case instantiating
	 *         a {@link Logger} ({@link RuntimeLogger}) failed
	 */
	public static RuntimeLogger getInstance( String aLoggerBeanId ) {
		if ( _runtimeLoggerSingleton == null ) {
			synchronized ( SpringRuntimeLoggerSingleton.class ) {
				if ( _runtimeLoggerSingleton == null ) {
					if ( aLoggerBeanId == null || aLoggerBeanId.isEmpty() ) {
						aLoggerBeanId = RUNTIME_LOGGER_BEAN;
					}
					final Resource theFactoryContextResource = new ClassPathResource( RUNTIME_LOGGER_CONTEXT );
					final BeanLookupFactory<String> theLookupFactory;
					try {
						theLookupFactory = new SpringBeanFactory( new URI[] { theFactoryContextResource.getURI() } );
					}
					catch ( IOException e ) {
						throw new LoggerInstantiationRuntimeException( "Unable to instantiate the runtime logger singleton from spring context file <{0}> for namespace (bean) <{1}>!", RUNTIME_LOGGER_CONTEXT, aLoggerBeanId, e );
					}
					_runtimeLoggerSingleton = theLookupFactory.create( aLoggerBeanId );
				}
			}
		}
		return _runtimeLoggerSingleton;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getName() {
		return _runtimeLoggerSingleton.getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LogPriority getLogPriority() {
		return _runtimeLoggerSingleton.getLogPriority();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aMarker, String aMessage ) {
		_runtimeLoggerSingleton.log( aMarker, aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aPriority, String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.log( aPriority, aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aMarker, String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.log( aMarker, aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void log( LogPriority aPriority, String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.log( aPriority, aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLog( LogPriority aPriority ) {
		return _runtimeLoggerSingleton.isLog( aPriority );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String aMessage ) {
		_runtimeLoggerSingleton.trace( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void trace( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.trace( aMessage, aArguments );
	}

	@Override
	public boolean isLogTrace() {
		return _runtimeLoggerSingleton.isLogTrace();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String aMessage ) {
		_runtimeLoggerSingleton.debug( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void debug( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.debug( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogDebug() {
		return _runtimeLoggerSingleton.isLogDebug();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String aMessage ) {
		_runtimeLoggerSingleton.info( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void info( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.info( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogInfo() {
		return _runtimeLoggerSingleton.isLogInfo();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void notice( String aMessage ) {
		_runtimeLoggerSingleton.notice( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void notice( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.notice( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogNotice() {
		return _runtimeLoggerSingleton.isLogNotice();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage ) {
		_runtimeLoggerSingleton.warn( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.warn( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.warn( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void warn( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.warn( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogWarn() {
		return _runtimeLoggerSingleton.isLogWarn();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage ) {
		_runtimeLoggerSingleton.error( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.error( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.error( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void error( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.error( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogError() {
		return _runtimeLoggerSingleton.isLogError();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage ) {
		_runtimeLoggerSingleton.critical( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.critical( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.critical( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void critical( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.critical( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogCritical() {
		return _runtimeLoggerSingleton.isLogCritical();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage ) {
		_runtimeLoggerSingleton.alert( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.alert( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.alert( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void alert( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.alert( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogAlert() {
		return _runtimeLoggerSingleton.isLogAlert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage ) {
		_runtimeLoggerSingleton.panic( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage, Object... aArguments ) {
		_runtimeLoggerSingleton.panic( aMessage, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage, Throwable aThrowable ) {
		_runtimeLoggerSingleton.panic( aMessage, aThrowable );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void panic( String aMessage, Throwable aThrowable, Object... aArguments ) {
		_runtimeLoggerSingleton.panic( aMessage, aThrowable, aArguments );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isLogPanic() {
		return _runtimeLoggerSingleton.isLogPanic();
	}
}
