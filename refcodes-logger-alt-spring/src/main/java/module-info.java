module org.refcodes.logger.alt.spring {
	requires transitive org.refcodes.exception;
	requires org.refcodes.factory;
	requires org.refcodes.factory.alt.spring;
	requires spring.core;
	requires transitive org.refcodes.logger;

	exports org.refcodes.logger.alt.spring;
}
