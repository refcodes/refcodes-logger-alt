# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact provides a [`SpringRuntimeLoggerSingleton`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-spring/latest/org.refcodes.logger.alt.spring/org/refcodes/logger/alt/spring/SpringRuntimeLoggerSingleton.html) as sub-type of the [`RuntimeLogger`](https://www.javadoc.io/doc/org.refcodes/refcodes-logger/latest/org.refcodes.logger/org/refcodes/logger/RuntimeLogger.html) type of the  [`refcodes-logger`](https://www.metacodes.pro/refcodes/refcodes-logger) toolkit to be configured (and injected) as [`spring framework`](https://spring.io/projects/spring-framework) bean.***

## Getting started ##

> Please refer to the [refcodes-logger: Fancy runtime-logs and highly scalable cloud logging](https://www.metacodes.pro/refcodes/refcodes-logger) documentation for an up-to-date and detailed description on the usage of this artifact.

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-logger-alt-spring</artifactId>
		<groupId>org.refcodes</groupId>
    	<version>3.0.0</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-logger-alt/src/master/refcodes-logger-alt-spring). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-logger-alt-spring).

## Contribution guidelines ##

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.